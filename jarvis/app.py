from flask import Flask 
from routes.bff.controller import bff_ressource
from routes.health.controller import health_ressource
from routes.agent.controller import agent_ressource
from routes.about.controller import about_ressource
from flask_cors import CORS

app = Flask(__name__)
cors = CORS(
			app, 
			resources={r"/api/*": {"origins": "*"}},
		)

# sanity check
app.register_blueprint(bff_ressource)
app.register_blueprint(health_ressource)

# app 
app.register_blueprint(agent_ressource)
app.register_blueprint(about_ressource)
if __name__ == '__main__':
	app.run(host='0.0.0.0', port=4500,  debug=True)
