from flask import Blueprint, jsonify, Response, stream_with_context, request
from llm.sql.executor import sql_agent_executor, sql_agent_streaming_executor

agent_ressource = Blueprint("agent", __name__, url_prefix="/api/agent")
import langchain

langchain.debug = True


@agent_ressource.route("", methods=["GET"])
def agent_dev():
    try:
        # return Response(
        # 	stream_with_context(
        # 		 agent_executor.stream(
        # 			{"input" :"How many items are not completed in the database?"},
        # 			#config ={"callbacks":[langfuse_handler]}
        # 		)
        # 	),
        # 	mimetype='text/event-stream'
        # )
        # response ="test"
        response = sql_agent_executor.invoke(
            {"input": "How many items are not completed in the database?"}
        )
        # print(agent_executor.invoke({"input" :"How many completed ?"}))
        return jsonify({"message": response.get("output")})

    except Exception as e:
        print("eeeeeeeee", e)
        return jsonify({"message": "error"})


@agent_ressource.route("", methods=["POST"])
def agent_handler():
    try:
        data = request.get_json()
        return Response(
            stream_with_context(
                sql_agent_streaming_executor().stream(
                    {"input": data["input"]},
                )
            ),
            mimetype="text/event-stream",
        )
    except Exception as e:
        print("err ---->", e)
        return jsonify({"message": "error"})
