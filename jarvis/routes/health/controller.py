from flask import Blueprint, jsonify, request

health_ressource = Blueprint("health", __name__, url_prefix="/api/health")


# https://github.com/corydolphin/flask-cors/issues/289
# never put '/' nested  blueprint, cors will not work somehow
@health_ressource.route("", methods=["GET"])
def health_handler():
    return jsonify({"message": "Jarvis is up"})


@health_ressource.route("", methods=["POST"])
def check_health():
    data = request.get_json()
    print(data)
    data["_msg"] = "jarvis is healthy"
    return jsonify(data)
