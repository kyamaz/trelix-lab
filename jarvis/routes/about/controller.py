from flask import (
    Blueprint,
    jsonify,
    Response,
    stream_with_context,
    request,
)
from utils.scrapping.main import scrap, path, angular_sitemap_path
from llm.vector_store.pinecone import (
    store,
    get_vector_store,
)
from llm.vector_store.loader import embed_html
from llm.retriever.chat import chat_retriever_stream
import langchain
import json

langchain.debug = True

about_ressource = Blueprint("about", __name__, url_prefix="/api/about")


@about_ressource.route("/store", methods=["POST"])
def create_vector_store_handler():
    try:
        data = request.get_json()
        index_name = data["index_name"]
        if index_name is None:
            Exception("index_name is required")
        store = get_vector_store(index_name)

        if not store:
            raise Exception("fail to create vector store")

        return jsonify({"message": "url content successfully embedded"})
    except Exception as e:
        print("fail to create vector store", e)
        return jsonify({"message": "fail to create vector store"})


# manually embed the content
# path example utils/scrapping/content/
@about_ressource.route("/embedding", methods=["POST"])
def embed_store_handler():
    try:
        data = request.get_json()
        index_name = data["index_name"]
        if index_name is None:
            Exception("index_name is required")
        store(embed_html(path), index_name)
        return jsonify({"message": "url content successfully embedded"})
    except Exception as e:
        print("fail to parse url", e)
        return jsonify({"message": "fail to parse url"})


# scrap url, create vector store and embed the content
@about_ressource.route("/rag", methods=["POST"])
def embed_web_handler():
    try:
        data = request.get_json()
        url = data["url"]
        if url is None:
            Exception("url is required")
        index_name = data["index_name"]
        if index_name is None:
            Exception("index_name is required")
        scrap(url)
        store(embed_html(path), index_name)
        return jsonify({"message": "url content successfully embedded"})
    except Exception as e:
        print("fail to parse url", e)
        return jsonify({"message": "fail to parse url"})


# load resource sitemap
@about_ressource.route("/embed/source", methods=["GET"])
def embed_source_handler():
    try:
        with open(angular_sitemap_path, "r") as f:
            content = f.readlines()
            to_json = json.loads(content[0])
            result = [{"link": value, "label": key} for key, value in to_json.items()]
            return jsonify({"data": result})

    except Exception as e:
        print("fail to parse url", e)
        return jsonify({"message": "fail to parse url"})


@about_ressource.route("/query/stream", methods=["POST"])
def query_stream__handler():
    try:
        data = request.get_json()
        q = data["input"]
        index_name = data["index_name"]
        return Response(
            stream_with_context(
                chat_retriever_stream(index_name).stream(
                    {"question": q},
                )
            ),
            mimetype="text/event-stream",
        )

    except Exception as e:
        print("stream error", e)
        return jsonify({"message": "error"})


# @about_ressource.route('/query', methods=['POST'])
# def query__handler():
#     try:
#         data = request.get_json()
#         q = data['q']
#         resp = chat_retriever(q)
#         chat_h=resp['chat_history']
#         print(chat_h)
#         print(resp['answer'])
#         print(resp)
#         answer = resp['answer']
#         return jsonify({"message": answer})
#
#     except Exception as e:
#         print('eeeeeeeee', e)
#         return jsonify({"message": 'error'})
#
