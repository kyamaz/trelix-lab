from flask import Blueprint, jsonify, request
import requests

bff_url = "http://server:4000/api/"

# simple routes  to  check communication with bff

bff_ressource = Blueprint("bff", __name__, url_prefix="/api/bff/health")


def check_health():
    response = requests.get(f"{bff_url}health")
    data = response.json()
    data["_msg"] = "jarvis is healthy"
    return jsonify(data)


def check_health_post():
    data = request.get_json()
    response = requests.post(f"{bff_url}health", json=data)
    return response.json()


# https://github.com/corydolphin/flask-cors/issues/289
# never put '/' nested  blueprint, cors will not work somehow
@bff_ressource.route("", methods=["GET", "POST"])
def bff_handler():
    if request.method == "GET":
        return check_health()

    if request.method == "POST":
        return check_health_post()
