import os
from bs4 import BeautifulSoup
from urllib.parse import urlparse
from collections import defaultdict
import json
import requests
# COPY PASTA from  https://github.com/jasonrobwebster/langchain-webscraper-demo/

normalized_path = "utils/scrapping/content/"
path = "./utils/scrapping/content/"
angular_sitemap_path = "./utils/scrapping/angular_content/sitemap.json"


def cleanUrl(url: str):
    return url.replace("https://", "").replace("/", "-").replace(".", "_")


def get_response_and_save(url: str):
    response = requests.get(url)
    if not os.path.exists(path):
        os.mkdir(path)
    parsedUrl = cleanUrl(url)
    with open(path + parsedUrl + ".html", "wb") as f:
        f.write(response.content)
    return response


def scrape_links(
    scheme: str,
    origin: str,
    path: str,
    depth=2,
    sitemap: dict = defaultdict(lambda: ""),
):
    if not path.startswith("/"):
        path = "/" + path

    siteUrl = scheme + "://" + origin + path
    print(scheme, origin, path)
    cleanedUrl = cleanUrl(siteUrl)

    if depth < 0:
        return
    if sitemap[cleanedUrl] != "":
        return

    sitemap[cleanedUrl] = siteUrl

    print(siteUrl, cleanedUrl)
    response = get_response_and_save(siteUrl)
    soup = BeautifulSoup(response.content, "html.parser")
    links = soup.find_all("a")

    for link in links:
        href = urlparse(link.get("href"))
        if (href.netloc != origin and href.netloc != "") or (
            href.scheme != "" and href.scheme != "https"
        ):
            continue
        scrape_links(
            href.scheme or "https",
            href.netloc or origin,
            href.path,
            depth=depth - 1,
            sitemap=sitemap,
        )
    return sitemap


def scrap(url: str):
    parsed_url = urlparse(url)
    print(parsed_url, url)
    sitemap = scrape_links(
        parsed_url.scheme, parsed_url.netloc, parsed_url.path, depth=2
    )
    with open(f"{path}sitemap.json", "w") as f:
        f.write(json.dumps(sitemap))
