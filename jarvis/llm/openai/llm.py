import os
from langchain_openai import OpenAI

llm = OpenAI(
 	openai_api_key=os.environ.get("OPENAI_API_KEY"),
)
