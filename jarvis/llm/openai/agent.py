from langchain.agents import create_openai_tools_agent
from .chat import streaming_chat
from llm.sql.tools import sql_tools
from llm.sql.prompt import prompt

sql_agent = create_openai_tools_agent(
    llm=streaming_chat,
    tools=sql_tools,
    prompt=prompt,
)
