from langchain_openai import ChatOpenAI

streaming_chat = ChatOpenAI(
    streaming=True,
)
await_chat = ChatOpenAI(
    streaming=False,
)
