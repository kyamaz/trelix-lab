import pinecone
import os
import time
from langchain_pinecone import Pinecone
from pinecone import ServerlessSpec

# from langchain_pinecone import PineconeVectorStore
from llm.openai.embedding import embedding_model

pc = pinecone.Pinecone(
    api_key=os.getenv("PINECONE_API_KEY"),
    environment=os.getenv("PINECONE_ENV_NAME"),
)

# vector_store = Pinecone.from_existing_index(
#     index_name=os.environ["PINECONE_INDEX_NAME"],
#     embedding=embedding_model,
#     namespace="jarvis",
# )


def get_vector_store(index_name: str, create_index: bool = True):
    existing_indexes = [index_info["name"] for index_info in pc.list_indexes()]
    if index_name not in existing_indexes:
        if not create_index:
            return
        pc.create_index(
            name=index_name,
            dimension=1536,
            metric="cosine",
            spec=ServerlessSpec(cloud="aws", region="us-east-1"),
        )
        while not pc.describe_index(index_name).status["ready"]:
            print("Waiting for Pinecone index to be ready...")
            time.sleep(1)

    return Pinecone.from_existing_index(
        index_name=index_name,
        embedding=embedding_model,
    )


# k is the number of documents to return
def build_retriever(k: int, index_name: str):
    vector_store = get_vector_store(index_name=index_name, create_index=False)
    if not vector_store:
        print("Vector store not found. Please create it first")
        return
    search_kwargs = {"k": k}
    return vector_store.as_retriever(search_kwargs=search_kwargs)


def store(docs, index_name):
    vector_store = get_vector_store(index_name)
    if not vector_store:
        return
    vector_store.add_documents(docs)
    return vector_store
