from langchain.chains import ConversationalRetrievalChain
from llm.openai.chat import streaming_chat, await_chat
from langchain.memory import ConversationBufferMemory
from langchain_community.chat_message_histories import ChatMessageHistory
from llm.streaming.retrival import StreamingRetrievalChain
from llm.vector_store.pinecone import build_retriever
from llm.tracing.main import langfuse_handler
# TODO retrieve  by doc_id / filter by a specific ressource
# retriever = RedundantFilterRetriever(
# 	embeddings=embedding_model,
# 	chroma=db,
# )


memory_chain = ConversationBufferMemory(
    chat_memory=ChatMessageHistory(),
    memory_key="chat_history",
    return_messages=True,
    output_key="answer",
)

# chat_retriever = ConversationalRetrievalChain.from_llm(
#     llm=streaming_chat,
#     memory=memory_chain,
#     retriever=retriever,
#     condense_question_llm=await_chat,
# )


def chat_retriever_stream(index_name: str):
    retriever = build_retriever(k=3, index_name=index_name)
    return StreamingRetrievalChain.from_llm(
        llm=streaming_chat,
        memory=memory_chain,
        retriever=retriever,
        condense_question_llm=await_chat,
        callbacks=[langfuse_handler],
    )
