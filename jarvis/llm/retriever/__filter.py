from langchain_core.retrievers import BaseRetriever
from langchain_core.embeddings import Embeddings
from langchain_community.vectorstores import Chroma
# ONLY FOR CHROMA

class RedundantFilterRetriever(BaseRetriever):
    """Retriever that filters documents based on a condition."""
    embeddings:Embeddings
    chroma:Chroma

    def get_relevant_documents(self, query: str) -> list[str]:
        emb=  self.embeddings.embed_query(query)
        return self.chroma.max_marginal_relevance_search_by_vector(
            embedding=emb,
            lambda_mult=0.7,
        )
       
    async def aget_relevant_documents(self, query: str) -> list[str]:
        documents = await self.retriever.aget_relevant_documents(query)
        return documents
