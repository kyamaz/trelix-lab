from langchain_core.callbacks.base import BaseCallbackHandler
from langchain_core.outputs.llm_result import LLMResult
from queue import Queue
from threading import Thread
from flask import current_app
from langchain.agents import AgentExecutor
from langchain.chains import ConversationalRetrievalChain


class ChainStreamingtHandler(BaseCallbackHandler):
    def __init__(self, queue):
        self.queue = queue
        self.streaming_run_ids = set()

    def on_chat_model_start(self, serialized, messages, run_id, **kwargs):
        if kwargs["invocation_params"]["stream"]:
            self.streaming_run_ids.add(run_id)

    def on_llm_new_token(self, token: str, **kwargs):
        self.queue.put(token)

    def on_llm_end(self, response: LLMResult, **kwargs):
        run_id = kwargs.get("run_id")
        print("run_id", run_id, kwargs)
        if run_id in self.streaming_run_ids:
            print("clear queue")
            self.queue.put(None)
            self.streaming_run_ids.remove(run_id)

    def on_llm_error(self, error: Exception, **kwargs):
        self.queue.put(None)


class StreameableChain:
    def stream(self, input, **kwargs):
        queue = Queue()
        handler = ChainStreamingtHandler(queue)

        def task(app_context):
            app_context.push()
            self(input, callbacks=[handler])

        Thread(target=task, args=[current_app.app_context()]).start()

        while True:
            token = queue.get()

            if token is None:
                print("clear queue")
                break

            yield token


class StreamingRetrievalChain(StreameableChain, ConversationalRetrievalChain):
    pass


# class StreamingtAgentExecutor(StreameableChain, AgentExecutor):
# 	pass
#


class StreamingtHandler(BaseCallbackHandler):
    def __init__(self, queue):
        self.queue = queue

    def on_llm_new_token(self, token: str, **kwargs):
        self.queue.put(token)

    def on_llm_end(self, response: LLMResult, **kwargs):
        generations = response.generations
        for gen in generations:
            for det in gen:
                finish_reason = det.generation_info.get("finish_reason")
                if finish_reason == "stop":
                    print("clear queue")
                    self.queue.put(None)

    def on_llm_error(self, error: Exception, **kwargs):
        self.queue.put(None)


class StreamingtAgentExecutor(AgentExecutor):
    def stream(self, input, **kwargs):
        queue = Queue()
        handler = StreamingtHandler(queue)

        def task(app_context):
            app_context.push()
            self(input, callbacks=[handler])

        Thread(target=task, args=[current_app.app_context()]).start()

        while True:
            token = queue.get()

            if token is None:
                break

            yield token
