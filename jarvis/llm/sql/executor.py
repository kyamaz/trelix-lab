from .tools import sql_tools
from llm.openai.agent import sql_agent
from .memory import memory
from llm.streaming.agent import StreamingtAgentExecutor
from langchain.agents import AgentExecutor
from llm.tracing.main import langfuse_handler


def sql_agent_streaming_executor():
    return StreamingtAgentExecutor(
        agent=sql_agent,
        tools=sql_tools,
        memory=memory,
        callbacks=[langfuse_handler],
    )


sql_agent_executor = AgentExecutor(
    agent=sql_agent,
    tools=sql_tools,
    memory=memory,
)
