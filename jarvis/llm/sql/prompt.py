from .tools import get_db_schema

from langchain_core.prompts import (
    ChatPromptTemplate,
    HumanMessagePromptTemplate,
    MessagesPlaceholder,
)
from langchain_core.messages import (
    SystemMessage,
)

prompt = ChatPromptTemplate.from_messages(
    [
        SystemMessage(
            content=(
                "You are an Ai assistant that have access to a sqlite database.\n"
                f"the database has tables of {get_db_schema().get('tables_names')}.\n"
                "Do not make any assumptions about what tables exits or what columns exist.\n"
                "Instead, use 'describe_tables' function"
            )
        ),
        MessagesPlaceholder(variable_name="chat_history"),
        HumanMessagePromptTemplate.from_template("{input}"),
        MessagesPlaceholder(variable_name="agent_scratchpad"),
    ]
)
