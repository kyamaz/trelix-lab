from typing import Dict, Tuple, Any
from langchain.tools import Tool, StructuredTool
from langchain.pydantic_v1 import BaseModel
import sqlite3, json, requests

query_url = "http://server:4000/api/query"


def get_db_schema() -> Dict:
    db_data = requests.get(f"{query_url}/schema").json().get("data")
    tables = "\n".join(db_data.get("tables_names"))
    return {"tables_names": tables, "schema": db_data.get("schema")}


def query_db(query: str) -> Tuple[Any]:
    try:
        data = {}
        data["query"] = query
        json_data = json.dumps(data)
        response = requests.post(query_url, json=json_data)
        resp = response.json().get("data")
        if resp is None:
            error = response.json().get("msg")
            if error is not None:
                return (f"the fellowing error occured: {error}",)
            return ("No data found",)
        return (resp,)
    except sqlite3.OperationalError as e:
        return (f"the fellowing error occured: {e}",)


def run_q(query: str) -> Tuple[Any]:
    """Run a sqlite query"""
    _res = query_db(query)
    return _res


class RunQueryArgs(BaseModel):
    query: str


query_sqlite_tool = Tool.from_function(
    func=run_q,
    name="run_sqlite_query",
    description="run a sqlite query",
    args_schema=RunQueryArgs,
)


class DescribeTablesArgs(BaseModel):
    table_names: str


# TODO func not needed as  it is handled by the api
def desc_table(table_names: str):
    return get_db_schema().get("schema")


describe_tables_tool = StructuredTool.from_function(
    func=desc_table,
    name="describe_tables",
    description="Given a list of table names,return the schema of the tables",
    args_schema=DescribeTablesArgs,
)

sql_tools = [
    query_sqlite_tool,
    describe_tables_tool,
]
