import { createSortable } from "@thisbeyond/solid-dnd";
import { For, VoidComponent, } from "solid-js";
import { Item, ItemOverlay } from "./Item";
import type { ItemApi, ColApi } from "../../model";
import { action, useAction } from "@solidjs/router";
/**
 * avoid item  id colision  by offsetting col id
 **/
function create_col_id(id: number) {
  return id + 1024;
}
function read_col_id(id: number) {
  return id - 1024;
}

const create_card = action(async (id: number) => {
  "use server"
  fetch(`${import.meta.env.VITE_API_DOMAIN}/api/item`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      title: 'new task',
      content: null,
      columnId: id
    })
  });
});

const GroupOverlay: VoidComponent<{ title: string; items: ItemApi[] }> = (
  props
) => {
  return (
    <div>
      <div class="column-header">{props.title}</div>
      <div class="column bg-gray-100">
        <For each={props.items}>
          {(item) => <ItemOverlay title={item.title} />}
        </For>
      </div>
    </div>
  );
};
function Group({ col }: { col: ColApi }) {
  const sortable = createSortable(create_col_id(col.id), { type: "group" });
  const create_action = useAction(create_card)
  return <ul ref={sortable.ref}
    class="flex flex-1 gap-4 overflow-auto flex-col bg-gray-200 rounded-lg py-2">
    <li class="pt-2 px-4 flex  justify-between">
      <h2 class="font-semibold"> {col.name} </h2>
      <button type="button" onClick={() => create_action(col.id)}> new card</button>
    </li >
    <li>
      <ul class="px-4 grid gap-4">
        {
          col.items.map(item => {
            return <Item item={item} />
          })
        }
      </ul>
    </li>
  </ul >
}

export { Group, GroupOverlay, read_col_id, create_col_id }
