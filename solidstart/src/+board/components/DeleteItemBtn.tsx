import { action, cache, reload, useAction, useNavigate, useSubmission } from "@solidjs/router";

const delete_item = action(async (id: number) => {
  "use server"


  await fetch(`${import.meta.env.VITE_API_DOMAIN}/api/item/${id.toString()}`, {
    method: "DELETE",
  })
});

function DeleteItemBtn({ id }: { id: number }) {
  const delete_action = useAction(delete_item)
  const deleting = useSubmission(delete_item, (input) => {
    return input.some(x => x === id)
  });
  //TDOO optomistic cache update
  // createEffect(async () => {
  //   if (deleting.pending) {
  //     console.log("deleting")
  //     cache.set('board', [])
  //   }
  // })
  return (<button type="button" onClick={() => delete_action(id)}>
    {deleting.pending ? "deleting" : "delete"}
  </button>
  );
}

export { DeleteItemBtn }
