import { ItemApi } from "../../model";
import { action, redirect } from "@solidjs/router";
import { Accessor, } from "solid-js";

const updateItemAction = action(async (formData: FormData) => {
  "use server";
  const title = formData.get('title');
  const content = formData.get('content') ?? null;
  const id = formData.get('id');
  const boardId = formData.get('boardId');
  if (!title || !id) {
    return new Error('title and id is required')
  }

  const response = await fetch(`${import.meta.env.VITE_API_DOMAIN}/api/item/${id}`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      title,
      content
    }),
  })
  if (!response.ok) {
    return new Error('Failed to update item')
  }

  throw redirect(`/board/${boardId}`)

})


export function EditItem({ item }: { item: Accessor<ItemApi | undefined> }) {
  return <form class="flex flex-col mx-auto  w-56"
    action={updateItemAction}
    method="post">
    <input type="text" name="id" value={item()?.id} hidden />
    {/* @ts-ignore todo  fix rapu return  type*/}
    <input type="text" name="boardId" value={item()?.boardId} hidden />

    <label class="w-full inline-block pt-6" for="title-input">title:</label>
    <input class="w-full  border-gray-200 h-10 px-1 border  rounded-md" type="text"
      name="title"
      id="title-input"
      value={item()?.title} />

    <label class="w-full inline-block pt-6" for="field-content" >content:</label>
    <textarea class="w-full  border-gray-200  px-1 border  rounded-md"
      rows="3"
      name="content"
      id="field-content"
      value={item()?.content}></textarea>

    <button class="mt-6 py-2 bg-purple-500 hover:bg-purple-600 text-white font-medium rounded-md " type="submit">Submit</button>
  </form>;
}
