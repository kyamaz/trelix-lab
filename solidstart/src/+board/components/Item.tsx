import type { ItemApi } from "../..//model";
import { createSortable } from "@thisbeyond/solid-dnd";
import { VoidComponent } from "solid-js";
import { DeleteItemBtn } from "./DeleteItemBtn";
import { render } from "@solidjs/testing-library";
const ItemOverlay: VoidComponent<{ title: string }> = (props) => {
  return <div class="h-24  bg-white  rounded-md  shadow-2xl">
    <h3 class="font-medium">{props.title}</h3>
  </div>;
};
function Item({ item }: { item: ItemApi }) {
  const sortable = createSortable(item.id, {
    type: "item",
    col_id: item.column_id,
    position: item.position,
    title: item.title,
  })
  return (
    <li>
      {/* @ts-ignore solidjs dirctive  issue */}
      <div use:sortable
        class="py-2 px-4 bg-white rounded-md  min-h-24">
        <div class="flex gap-x-4 justify-end">
          <DeleteItemBtn id={item.id} />
          <a href={`/board/${item.column_id}/item/${item.id}`}>edit </a>
        </div>
        <h3 class="font-medium">{item.title}</h3>
        <p>{item.content}</p>
      </div>
    </li>
  );
}

export { Item, ItemOverlay };



if (import.meta.vitest) {
  const { it, expect, describe } = import.meta.vitest;
  // Mock the useLocation hook

  describe('Nav', () => {
    it('should render', async () => {
      const { queryByText } = render(() => <ItemOverlay title="test" />);
      const overlay_title = queryByText("test");
      expect(overlay_title).toBeTruthy();
    });
  })
}
