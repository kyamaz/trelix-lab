import { action, useAction } from "@solidjs/router";
import { CollisionDetector, DragDropProvider, DragDropSensors, DragEventHandler, DragOverlay, Draggable, Droppable, SortableProvider, closestCorners } from "@thisbeyond/solid-dnd";
import { createSignal } from "solid-js";
import { Group, GroupOverlay, create_col_id, read_col_id } from "./Group";
import { ItemOverlay } from "./Item";
import type { ColApi } from "../../model"

const moveItem = action(async (payload) => {
  await fetch(`${import.meta.env.VITE_PUBLIC_API_DOMAIN}/api/item/move`, {
    method: "PATCH",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(payload),
  });
});

function Board({ columns }: { columns: () => ColApi[] }) {
  const moveItemAction = useAction(moveItem);
  const [insert, setInsert] = createSignal('before');
  const groupIds = () => {
    return columns()?.map((col: ColApi) => create_col_id(col.id))
  }

  const isSortableGroup = (sortable: Draggable | Droppable) => sortable.data.type === "group";


  function move(
    draggable: Draggable,
    droppable: Droppable,
  ) {
    if (!draggable || !droppable) {
      return
    }

    if (draggable.data.type === "group" && droppable.data.type === "group") {
      //TODO yeah sure
      console.warn("cant move group to group.weird colision handeling")
      return
    }

    if (draggable.id === droppable.id && droppable.data.type === "item") {
      // item did not changed position
      return
    }
    const target = draggable.id === droppable.id || droppable.data.type === "group" ?
      {
        id: undefined,
        col_id: read_col_id(droppable.id as unknown as number),
        position: undefined
      }
      :
      {
        id: droppable.id,
        col_id: droppable.data.col_id,
        position: droppable.data.position,
      }

    const payload = {
      from: { id: draggable.id, position: draggable.data.position, column_id: draggable.data.col_id },
      to: { id: target.id, position: target.position, column_id: target.col_id },
      insert: insert()
    };

    moveItemAction(payload);
  };
  const onDragEnd: DragEventHandler = ({ draggable, droppable }) => {
    move(draggable, droppable!);
  }

  const closestEntity: CollisionDetector = (draggable, droppables, context) => {
    const closestItem = closestCorners(
      draggable,
      droppables,
      context
    );

    if (closestItem?.data.type === "group") {
      return closestItem;
    }

    const position = draggable.transformed.y > closestItem!.transformed.y ? 'after' : 'before';
    setInsert(position);

    return closestItem;
  }
  return (
    <DragDropProvider
      collisionDetector={closestEntity}
      onDragEnd={onDragEnd}>
      <DragDropSensors />
      <main class="grid grid-cols-3 overflow-auto gap-6 w-full mx-auto h-[calc(100vh-50px)] px-6 pt-8 pb-16">
        <SortableProvider ids={[]}>
          {Array.isArray(columns()) && columns().map(col => <Group col={col} />)}
        </SortableProvider>
      </main>
      <DragOverlay>
        {(draggable) => {
          if (!draggable) {
            return null
          }
          const item = draggable.data
          return isSortableGroup(draggable) ? (
            <GroupOverlay title={item.title} items={[]} />
          ) : (
            <ItemOverlay title={item.title} />
          );
        }}
      </DragOverlay>
    </DragDropProvider>
  );
}

export { Board }
