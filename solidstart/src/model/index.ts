
interface ItemApi {
  id: number;
  column_id: number;
  position: number;
  title: string;
  content: string;
  board_id: number;
  completed: boolean;
  due_date: string | null;
}
interface ColApi {
  board_id: number;
  id: number;
  position: number;
  items: ItemApi[];
  name: string;
}

interface ApiResponse<T> {
  data: T
}

export type { ItemApi, ColApi, ApiResponse };
