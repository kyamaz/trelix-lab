import { useSubmission } from "@solidjs/router";
import { Accessor, For, Setter, createEffect, createSignal } from "solid-js";
import { action } from "@solidjs/router";
export namespace AiFormI {
  export interface Props {
    title: string;
    api_url: string;
    index_name?: string
  }
}

interface History {
  input: string;
  result: Accessor<string>;
  setResult: Setter<string>;
}
const inputId = 'ai-input';
const aiAction = (api_url: string, index_name: string | null = null) => {
  return action(async (formData: FormData) => {
    const input = formData.get(inputId);

    if (!input) {
      return
    }
    const response = await fetch(api_url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        input,
        index_name
      }),
    })
    return response.body?.getReader();
  }, 'aiAction')
}
export default function AiForm({ title, api_url, index_name }: AiFormI.Props) {
  const [history, setHistory] = createSignal<History[]>([]);
  const action = aiAction(api_url, index_name);
  const query = useSubmission<[FormData], ReadableStreamDefaultReader | undefined>(action);
  let input: HTMLTextAreaElement;
  createEffect(async () => {
    if (query.pending) {
      window.scroll({
        top: 0,
        left: 0,
        behavior: 'smooth'
      });
      input.value = '';
      const q = (query.input[0] as FormData).get(inputId);
      const [result, setResult] = createSignal('');
      setHistory((prevData) => {
        return [{
          input: q,
          result,
          setResult
        } as History].concat(prevData);
      });
    }
    if (query.result) {
      const decoder = new TextDecoder();
      if (!query.result) {
        return;
      }
      const { setResult } = history().at(0) ?? { setResult: null };
      if (!setResult) {
        return;
      }
      while (true) {
        const { done, value } = await query.result.read();
        if (done) {
          return;
        };
        setResult((prev: string) => prev + decoder.decode(value));
      }
    }
  })
  return <section class="w-full">
    <h1 class="text-purple-700 font-medium text-xl">
      {title}
    </h1>
    <ul class="pt-4">
      <For each={history()}>
        {({ input, result }, index) => {
          return <div class="pt-4 flex flex-col gap-1">
            <pre class="border bg-gray-50 rounded-md px-4 py-2 text-start text-gray-700 whitespace-pre-wrap">
              {query.pending && index() === 0 ? 'querying...' : result()}
            </pre>
            <p class="border bg-gray-50 rounded-md px-4  py-2 w-fit self-end italic text-gray-700">
              {input}
            </p>
          </div>
        }}
      </For>
    </ul>
    <form class="flex flex-col mx-auto sticky bottom-0  bg-white  border rounded-md px-2 py-2"
      action={action}
      method="post">
      <label class="text-start pb-1 " for={inputId}>
        question:
      </label>
      <textarea class="w-full  border-gray-200 px-1 border rounded-md"
        placeholder="ask anything about board"
        rows="2"
        id={inputId}
        name={inputId}
        ref={(el) => input = el}
      />
      <button class="mt-6 py-2 bg-purple-500 hover:bg-purple-600 text-white font-medium rounded-md "
        type="submit">
        Submit
      </button>
    </form>
  </section>
}



