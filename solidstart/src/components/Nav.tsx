import { useLocation } from "@solidjs/router";
import { screen, render } from "@solidjs/testing-library";
import { createRoot } from "solid-js";

export default function Nav() {
  const location = useLocation();
  const active = (path: string) =>
    path == location.pathname ? "border-white" : "border-transparent hover:border-purple-700";
  return (
    <nav class="bg-purple-600">
      <ul class="container flex items-center p-3 text-gray-200">
        <li role='navigation' class={`border-b-2 ${active("/")} mx-1.5 sm:mx-6`}>
          <a href="/">Home</a>
        </li>
        <li role='navigation' class={`border-b-2 ${active("/board")} mx-1.5 sm:mx-6`}>
          <a href="/board">Board</a>
        </li>
        <li role='navigation' class={`border-b-2 ${active("/board/ama")} mx-1.5 sm:mx-6`}>
          <a href="/board/assistant">Board assistant</a>
        </li>
        <li role='navigation' class={`border-b-2 ${active("/angular")} mx-1.5 sm:mx-6`}>
          <a href="/angular">Angular Assistant</a>
        </li>
      </ul>
    </nav>
  );
}


// in-source test suites
if (import.meta.vitest) {
  const { it, expect, describe } = import.meta.vitest;
  // Mock the useLocation hook
  vi.mock("@solidjs/router", async (importOriginal) => {
    const actual = await importOriginal();
    return {
      //@ts-expect-error silence  type error
      ...actual,
      useLocation: () => ({ pathname: "/" })
    };
  });

  describe('Nav', () => {
    it('should render', async () => {
      createRoot(async (dispose) => {
        const { queryByText } = render(() => <Nav />);
        const homeLink = queryByText("Home");
        const boardLink = queryByText("Board");
        const boardAssistantLink = queryByText("Board assistant");
        const angularLink = queryByText("Angular Assistant");

        expect(homeLink).toBeTruthy();
        expect(boardLink).toBeTruthy();
        expect(boardAssistantLink).toBeTruthy();
        expect(angularLink).toBeTruthy();
        const navItems = screen.getAllByRole('navigation');

        const homeItem = navItems.find(item => item.textContent === 'Home')
        expect(homeItem).toHaveClass('border-white');
        const boardItem = navItems.find(item => item.textContent === 'Board')
        expect(boardItem).not.toHaveClass('border-white');
        dispose();
      });
    })
  });
}
