import { cache, createAsync } from "@solidjs/router";
import { For } from "solid-js";

async function loadSource() {
  'use server'
  return fetch(`${import.meta.env.VITE_API_DOMAIN}/api/angular/source`)
    .then(res => res.json())
    .then(res => {
      return res?.data ?? [];
    })
}
const cachedSource = cache(loadSource, "ng_sitemap");
export default function NgSources() {
  const sources = createAsync<Record<string, string>[]>(() => cachedSource());
  return (
    <div>
      <h2>sources</h2>
      <ul class="list-none">
        <For each={sources()}>{(source =>
          <li class="text-left text-xs">
            <a href={source.link} target="_blank" class="text-blue-500 hover:underline  px-2 underline">
              {source.label}
            </a>
          </li>
        )}
        </For>
      </ul>
    </div>
  );
}
