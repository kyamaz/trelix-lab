import { render, } from "@solidjs/testing-library";
export default function HomePage() {
  return (
    <main class="text-center mx-auto text-gray-700 p-4">
      main page may be logg
    </main>
  );
}

// in-source test suites
if (import.meta.vitest) {
  const { it, expect, describe } = import.meta.vitest;

  describe('HomePage', () => {
    it('should render', async () => {

      const { queryByText } = render(() => <HomePage />);

      const homeText = queryByText("main page may be logg");

      expect(homeText).toBeTruthy();

    });
  })
}
