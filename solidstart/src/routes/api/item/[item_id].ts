import type { ItemApi, ApiResponse } from "../../../model";

export async function GET({ params: { item_id } }: { params: { item_id: string } }): Promise<ApiResponse<ItemApi>> {
  return fetch(`${import.meta.env.VITE_API_DOMAIN}/api/item/${item_id}`)
    .then(res => res.json())
}


if (import.meta.vitest) {
  const { it, expect, describe } = import.meta.vitest;
  const mock_data = {
    id: 1,
    column_id: 1,
    position: 1,
    title: 'item 1',
    content: "content 1",
    board_id: 1,
    completed: false,
    due_date: new Date(1, 1, 2022).toISOString()
  };
  describe('loadBoard', () => {
    it('fetches data from the API', async () => {
      const fetchMock = vi.fn(() => Promise.resolve({
        json: () => Promise.resolve(mock_data),
      }));
      vi.stubGlobal('fetch', fetchMock);

      await GET({ params: { item_id: '1' } });

      expect(fetchMock).toHaveBeenCalledTimes(1);
      expect(fetchMock).toHaveBeenCalledWith(`${import.meta.env.VITE_CLIENT_DOMAIN}/api/item/1`);
    });

    it('returns the fetched data', async () => {
      const fetchMock = vi.fn(() => Promise.resolve({
        json: () => Promise.resolve(mock_data),
      }));
      vi.stubGlobal('fetch', fetchMock);

      const result = await GET({ params: { item_id: '1' } });

      expect(result).toEqual(mock_data);
    });
  })
}
