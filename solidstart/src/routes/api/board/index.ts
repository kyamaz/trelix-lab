
type Column = {
  id: number;
  name: string;
  boardId: number;
}
type Board = {
  id: number;
  name: string;
  columns: Column[];
}

interface ApiResponse<T> {
  data: T
}

type BoardApi = ApiResponse<Board>;

export async function GET(): Promise<ApiResponse<BoardApi[]>> {
  return fetch(`${import.meta.env.VITE_API_DOMAIN}/api/board`)
    .then(res => res.json())
}
if (import.meta.vitest) {
  const { it, expect, describe } = import.meta.vitest;
  const mock_data = [
    { id: '1', name: 'Board 1' },
    { id: '2', name: 'Board 2' },
  ];
  describe('loadBoard', () => {
    it('fetches data from the API', async () => {
      const fetchMock = vi.fn(() => Promise.resolve({
        json: () => Promise.resolve(mock_data),
      }));
      vi.stubGlobal('fetch', fetchMock);

      await GET();

      expect(fetchMock).toHaveBeenCalledTimes(1);
      expect(fetchMock).toHaveBeenCalledWith(`${import.meta.env.VITE_CLIENT_DOMAIN}/api/board`);
    });

    it('returns the fetched data', async () => {
      const fetchMock = vi.fn(() => Promise.resolve({
        json: () => Promise.resolve(mock_data),
      }));
      vi.stubGlobal('fetch', fetchMock);

      const result = await GET();

      expect(result).toEqual(mock_data);
    });
  })
}

