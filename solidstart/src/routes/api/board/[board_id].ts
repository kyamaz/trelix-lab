import type { ApiResponse } from "../../../model";

//TODO move to server models
type Column = {
  id: number;
  name: string;
  boardId: number;
}
type Board = {
  id: number;
  name: string;
  columns: Column[];
}


type BoardApi = ApiResponse<Board>;

export async function GET({ params: { board_id } }: { params: { board_id: string } }): Promise<ApiResponse<BoardApi[]>> {
  return fetch(`${import.meta.env.VITE_API_DOMAIN}/api/board/`.concat(board_id, '/column'))
    .then(res => res.json())
}

if (import.meta.vitest) {
  const { it, expect, describe } = import.meta.vitest;
  const mock_data = [
    { id: '1', name: 'Board 1', items: [{ id: '1', title: 'item 1', content: 'content 1', column_id: '1', position: '1', boardId: '1' }] },
    { id: '2', name: 'Board 2', items: [{ id: '2', title: 'item 2', content: 'content 2', column_id: '2', position: '1', boardId: '2' }] },
  ];
  // Mock the router all functions  used in this page
  describe('loadBoard', () => {
    it('fetches data from the API', async () => {
      const fetchMock = vi.fn(() => Promise.resolve({
        json: () => Promise.resolve(mock_data),
      }));
      vi.stubGlobal('fetch', fetchMock);

      await GET({ params: { board_id: '1' } });

      expect(fetchMock).toHaveBeenCalledTimes(1);
      expect(fetchMock).toHaveBeenCalledWith(`${import.meta.env.VITE_CLIENT_DOMAIN}/api/board/1/column`);
    });

    it('returns the fetched data', async () => {
      const fetchMock = vi.fn(() => Promise.resolve({
        json: () => Promise.resolve(mock_data),
      }));
      vi.stubGlobal('fetch', fetchMock);

      const result = await GET({ params: { board_id: '1' } });

      expect(result).toEqual(mock_data);
    });
  })
}


