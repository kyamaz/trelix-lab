
import { createRoot } from "solid-js";
import NgSources from "../../+angular/components/NgSources";
import AiForm from "../../components/AiForm";
import { render, screen } from "@solidjs/testing-library";

export default function AiPage() {
  return (
    <main class="text-gray-700 p-4 text-center grid grid-cols-[70%_auto] gap-4 ">
      <AiForm title="Angular assistant"
        api_url={`${import.meta.env.VITE_PUBLIC_API_DOMAIN}/api/angular/query`}
        index_name="angular"
      />
      <div>
        <a class="text-blue-500 underline text-sm" href="/angular/doc">What is RAG</a>
        <NgSources />
      </div>
    </main>
  );
}


if (import.meta.vitest) {
  const { it, expect, describe } = import.meta.vitest;
  // Mock the router all functions  used in this page
  vi.mock("@solidjs/router", async (importOriginal) => {
    const actual = await importOriginal();
    return {
      //@ts-expect-error silence  type error
      ...actual,
      loadSource: vi.fn(),
      cache: vi.fn().mockReturnValue(() => { }),
      action: vi.fn(),
      useAction: vi.fn(),
      useSubmission: vi.fn().mockReturnValue(() => ({ pending: false, result: undefined, input: [] }))
    };
  });


  describe('Angular assistant Page', () => {
    it('render ai form', () => {
      createRoot((dispose) => {
        //@ts-expect-error ignore  type error
        render(() => <AiPage params={{ url: 'http://localhost' }} />);
        // Wait for the async data to load and check the rendered content
        const aiInput = screen.getByRole('textbox', { name: 'question:' });
        expect(aiInput).not.toBeNull();

        dispose();
      });

    });

  });

};
