
import { RouteSectionProps, cache, createAsync, } from "@solidjs/router";
import { createRoot, } from "solid-js";
import { Board } from "../../../+board/components/Board";
import { render, screen } from "@solidjs/testing-library";
async function loadColumn(id: string) {
  try {

    return fetch(`${import.meta.env.VITE_CLIENT_DOMAIN}/api/board/${id}`)
      .then(res => res.json())
      .then(res => {
        return res?.data ?? []
      })
  } catch (err) {
    console.error(err)
    return []
  }
}
const cachedColumn = cache((id) => loadColumn(id), 'board')
export const route = {
  load: ({ params }: { params: { board_id: string } }) => {
    return cachedColumn(params.board_id)
  },
}

export default function BoardDetailPage({ params }: RouteSectionProps) {
  const columns = createAsync<any>(() => cachedColumn(params.board_id));
  return <Board columns={columns} />
}


if (import.meta.vitest) {
  const { it, expect, describe } = import.meta.vitest;
  // Mock data
  const mock_data = [
    { id: '1', name: 'Board 1', items: [{ id: '1', title: 'item 1', content: 'content 1', column_id: '1', position: '1', boardId: '1' }] },
    { id: '2', name: 'Board 2', items: [{ id: '2', title: 'item 2', content: 'content 2', column_id: '2', position: '1', boardId: '2' }] },
  ];
  // Mock the router all functions  used in this page
  vi.mock("@solidjs/router", async (importOriginal) => {
    const actual = await importOriginal();
    return {
      //@ts-expect-error silence  type error
      ...actual,
      action: vi.fn(),
      loadColumn: vi.fn(),
      cache: vi.fn(),
      useAction: vi.fn(),
      createAsync: vi.fn((fn) => fn),
      useSubmission: vi.fn().mockReturnValue(() => ({ pending: false, result: undefined, input: [] }))
    };
  });

  describe('loadBoard details', () => {
    it('fetches data from the API', async () => {
      const fetchMock = vi.fn(() => Promise.resolve({
        json: () => Promise.resolve({ data: [] }),
      }));
      vi.stubGlobal('fetch', fetchMock);

      await loadColumn("1");

      expect(fetchMock).toHaveBeenCalledTimes(1);
      expect(fetchMock).toHaveBeenCalledWith(`${import.meta.env.VITE_CLIENT_DOMAIN}/api/board/1`);
    });

    it('returns the fetched data', async () => {
      const fetchMock = vi.fn(() => Promise.resolve({
        json: () => Promise.resolve({ data: mock_data }),
      }));
      vi.stubGlobal('fetch', fetchMock);

      const result = await loadColumn("1");

      expect(result).toEqual(mock_data);
    });

    it('returns an empty array if the API returns null', async () => {
      const fetchMock = vi.fn(() => Promise.resolve({
        json: () => Promise.resolve({ data: null }),
      }));
      vi.stubGlobal('fetch', fetchMock);

      const result = await loadColumn("1");

      expect(result).toEqual([]);
    });
  });

  describe('Board details Page', () => {
    it('renders board items', async () => {
      vi.mocked(createAsync).mockReturnValue(() => mock_data);

      createRoot((dispose) => {
        //@ts-expect-error ignore  type error
        render(() => <BoardDetailPage params={{ board_id: '1' }} />);
        // Wait for the async data to load and check the rendered content
        expect(screen.getByText(mock_data[0].name)).toBeDefined();
        expect(screen.getByText(mock_data[1].name)).toBeDefined();
        expect(screen.getByText(mock_data[0].items[0].title)).toBeDefined();
        expect(screen.getByText(mock_data[1].items[0].title)).toBeDefined();

        dispose();
      });


    });

    it('renders empty list when no boards are returned', async () => {
      vi.mocked(createAsync).mockReturnValue(() => []);
      createRoot((dispose) => {
        //@ts-expect-error ignore  type error
        render(() => <BoardDetailPage params={{ board_id: '1' }} />);
        // Wait for the async data to load and check the rendered content
        expect(screen.queryByRole('listitem')).toBeNull();
        dispose();
      });

    });
  });

};
