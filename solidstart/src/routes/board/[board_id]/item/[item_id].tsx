import { ItemApi } from "../../../../model";
import { RouteSectionProps, cache, createAsync } from "@solidjs/router";
import { EditItem } from "./../../../../+board/components/EditItem";
import { createRoot } from "solid-js";
import { render, screen, } from "@solidjs/testing-library";

async function loadItem(id: string) {
  return fetch(`${import.meta.env.VITE_CLIENT_DOMAIN}/api/item/${id}`)
    .then(res => res.json())
    .then(res => res?.data ?? {})
}
const cachedItem = cache((id) => loadItem(id), `item`);
export const route = {
  load: ({ params }: { params: { item_id: string } }) => {
    return cachedItem(params.item_id);
  },
}

export default function ItemDetailPage({ params, }: RouteSectionProps) {
  const item = createAsync<ItemApi>(() => cachedItem(params.item_id));
  return <EditItem item={item} />
}


if (import.meta.vitest) {
  const { it, expect, describe } = import.meta.vitest;
  // Mock the router all functions  used in this page
  vi.mock("@solidjs/router", async (importOriginal) => {
    const actual = await importOriginal();
    return {
      //@ts-expect-error silence  type error
      ...actual,
      load: vi.fn(),
      action: vi.fn(),
      cache: vi.fn(),
      useAction: vi.fn(),
      createAsync: vi.fn((fn) => fn),
      useSubmission: vi.fn().mockReturnValue(() => ({ pending: false, result: undefined, input: [] }))
    };
  });

  const mock_data = {
    id: 1,
    column_id: 1,
    position: 1,
    title: 'item 1',
    content: "content 1",
    board_id: 1,
    completed: false,
    due_date: new Date(1, 1, 2022).toISOString()
  };
  describe('loadBoard item', () => {
    it('fetches data from the API', async () => {
      const fetchMock = vi.fn(() => Promise.resolve({
        json: () => Promise.resolve({ data: [] }),
      }));
      vi.stubGlobal('fetch', fetchMock);

      await loadItem("1");

      expect(fetchMock).toHaveBeenCalledTimes(1);
      expect(fetchMock).toHaveBeenCalledWith(`${import.meta.env.VITE_CLIENT_DOMAIN}/api/item/1`);
    });

    it('returns the fetched data', async () => {
      const fetchMock = vi.fn(() => Promise.resolve({
        json: () => Promise.resolve({ data: mock_data }),
      }));
      vi.stubGlobal('fetch', fetchMock);

      const result = await loadItem("1");

      expect(result).toEqual(mock_data);
    });

    it('returns an empty object if the API returns null', async () => {
      const fetchMock = vi.fn(() => Promise.resolve({
        json: () => Promise.resolve({ data: null }),
      }));
      vi.stubGlobal('fetch', fetchMock);

      const result = await loadItem("1");

      expect(result).toEqual({});
    });
  });

  describe('Board item Page', () => {
    it('render board item  data  in a form', async () => {
      // Mock data
      vi.mocked(createAsync).mockReturnValue(() => mock_data);

      await createRoot(async (dispose) => {
        //@ts-expect-error ignore  type error
        render(() => <ItemDetailPage params={{ item_id: '1' }} />);
        // Wait for the async data to load and check the rendered content
        const titleInput = await screen.findByRole('textbox', { name: 'title:' });
        expect(titleInput).toBeDefined();
        expect(titleInput).toHaveValue(mock_data.title);

        const contentInput = await screen.findByRole('textbox', { name: 'content:' });
        expect(contentInput).toBeDefined();
        expect(contentInput).toHaveValue(mock_data.content);

        dispose();
      });

    });

  });

};
