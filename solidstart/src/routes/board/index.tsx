import { cache, createAsync } from "@solidjs/router";
import { render, screen, } from "@solidjs/testing-library";
import { createRoot } from "solid-js";
import { Mock } from "vitest";

async function loadBoard() {
  return fetch(`${import.meta.env.VITE_CLIENT_DOMAIN}/api/board`)
    .then(res => res.json())
    .then(res => res?.data ?? [])
}
const cachedBoard = cache(loadBoard,
  "board");
export const route = {
  load: () => cachedBoard
}

export default function BoardPage() {

  const boards = createAsync<any[]>(() => cachedBoard());
  return (
    <main class="text-center mx-auto text-gray-700 p-4">
      {boards()?.map(board => {
        return <ul>
          <li >
            <a href={"/board/".concat(board.id)}>
              {board.name}
            </a>
          </li>
        </ul>
      })}
    </main>
  );
}


if (import.meta.vitest) {
  const { it, expect, describe } = import.meta.vitest;

  // Mock the cachedBoard function
  vi.mock("@solidjs/router", async (importOriginal) => {
    const actual = await importOriginal();
    return {
      //@ts-expect-error silence  type error
      ...actual,
      loadBoard: vi.fn(),
      cache: vi.fn(),
      createAsync: vi.fn((fn) => fn),
    };
  });

  const mock_data = [
    { id: '1', name: 'Board 1' },
    { id: '2', name: 'Board 2' },
  ];
  describe('loadBoard', () => {
    it('fetches data from the API', async () => {
      const fetchMock = vi.fn(() => Promise.resolve({
        json: () => Promise.resolve({ data: [] }),
      }));
      vi.stubGlobal('fetch', fetchMock);

      await loadBoard();

      expect(fetchMock).toHaveBeenCalledTimes(1);
      expect(fetchMock).toHaveBeenCalledWith(`${import.meta.env.VITE_CLIENT_DOMAIN}/api/board`);
    });

    it('returns the fetched data', async () => {
      const fetchMock = vi.fn(() => Promise.resolve({
        json: () => Promise.resolve({ data: mock_data }),
      }));
      vi.stubGlobal('fetch', fetchMock);

      const result = await loadBoard();

      expect(result).toEqual(mock_data);
    });

    it('returns an empty array if the API returns null', async () => {
      const fetchMock = vi.fn(() => Promise.resolve({
        json: () => Promise.resolve({ data: null }),
      }));
      vi.stubGlobal('fetch', fetchMock);

      const result = await loadBoard();

      expect(result).toEqual([]);
    });
  });

  describe('BoardPage', () => {
    it('renders board links correctly', async () => {
      // Mock data
      (createAsync as Mock).mockReturnValue(() => mock_data);
      createRoot(async (dispose) => {
        render(() => <BoardPage />);
        // Wait for the async data to load and check the rendered content
        expect(screen.getByText(mock_data[0].name)).toBeDefined();
        expect(screen.getByText(mock_data[1].name)).toBeDefined();

        // Check if the links are correct
        const links = screen.getAllByRole('link');
        expect(links).toHaveLength(mock_data.length);
        expect(links[0]).toHaveAttribute('href', '/board/1');
        expect(links[1]).toHaveAttribute('href', '/board/2');

        dispose();
      });


    });

    it('renders empty list when no boards are returned', async () => {
      // Render the component
      (createAsync as Mock).mockReturnValue(() => []);
      createRoot(async (dispose) => {
        render(() => <BoardPage />);
        expect(screen.queryByRole('link')).toBeNull();
        dispose();
      });

    });
  });

};
