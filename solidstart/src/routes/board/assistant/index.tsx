import { render, screen, } from "@solidjs/testing-library";
import { createRoot } from "solid-js";
import AiForm from "../../../components/AiForm";

export default function AiPage() {
  return (
    <main class="text-gray-700 p-4 text-center grid grid-cols-[70%_auto] gap-4 ">
      <AiForm title="Board assistant"
        api_url={`${import.meta.env.VITE_PUBLIC_API_DOMAIN}/api/board/ask`}
      />
      <div>
        <a class="text-blue-500 underline text-sm" href="/board/assistant/doc">What is agent</a>
      </div>
    </main>
  );
}


if (import.meta.vitest) {
  const { it, expect, describe } = import.meta.vitest;
  // Mock the router all functions  used in this page
  vi.mock("@solidjs/router", async (importOriginal) => {
    const actual = await importOriginal();
    return {
      //@ts-expect-error silence  type error
      ...actual,
      action: vi.fn(),
      useAction: vi.fn(),
      useSubmission: vi.fn().mockReturnValue(() => ({ pending: false, result: undefined, input: [] }))
    };
  });


  describe('Assistant Page', () => {
    it('render ai form', async () => {
      createRoot(async (dispose) => {
        //@ts-expect-error ignore  type error
        render(() => <AiPage params={{ url: 'http://localhost' }} />);
        // Wait for the async data to load and check the rendered content
        const aiInput = screen.getByRole('textbox', { name: 'question:' });

        expect(aiInput).not.toBeNull();

        dispose();
      });

    });

  });

};
