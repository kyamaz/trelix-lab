import { render } from "@solidjs/testing-library";
import { createRoot } from "solid-js";

export default function NotFound() {
  return (
    <main class="text-center mx-auto text-gray-700 p-4">
      <h1 class="max-6-xs text-6xl text-sky-700 font-thin uppercase my-16">Not Found</h1>
      <p class="my-4">
        <a href="/" class="text-sky-600 hover:underline">
          Home
        </a>
      </p>
    </main>
  );
}

// in-source test suites
if (import.meta.vitest) {
  const { it, expect, describe } = import.meta.vitest;
  // Mock the useLocation hook
  vi.mock("@solidjs/router", async (importOriginal) => {
    const actual = await importOriginal();
    return {
      //@ts-expect-error silence  type error
      ...actual,
      useLocation: () => ({ pathname: "/notfound" })
    };
  });

  describe('NotFound Page', () => {
    it('should render', async () => {
      createRoot(async (dispose) => {
        const { queryByText } = render(() => <NotFound />);
        const homeLink = queryByText("Home");
        expect(homeLink).toBeTruthy();
        const pageNotFound = queryByText("Not Found");
        expect(pageNotFound).toBeTruthy();
        dispose();
      });
    })
  });
}
