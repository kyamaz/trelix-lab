/// <reference types="vitest" />
import { defineConfig } from "@solidjs/start/config";
import pkg from "@vinxi/plugin-mdx";
const { default: mdx } = pkg;

export default defineConfig({
  //ssr: true, //https://github.com/oven-sh/bun/issues/2644
  extensions: ["mdx", "md"],
  server: {
    prerender: {
      routes: [
        '/'
      ]
    }
  },
  vite: {
    plugins: [
      // Add and configure the `@vinxi/plugin-mdx` plugin in order to properly handle markdown files
      mdx.withImports({})({
        jsx: true,
        jsxImportSource: "solid-js",
        providerImportSource: "solid-mdx",
      }),
    ],
    server: {
      host: '0.0.0.0',
      hmr: {
        port: 3001,
        clientPort: 3001,
        host: "0.0.0.0",
      },
    },
    define: {
      'import.meta.vitest': 'undefined',
    },
    resolve: {
      conditions: ["development", "browser"],
    }
  },
});
