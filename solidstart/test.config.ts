/// <reference types="vitest" />
import { configDefaults, defineConfig, mergeConfig } from "vitest/config";
import appConfig from './app.config';
import solid from "vite-plugin-solid";
export default mergeConfig(
  appConfig,
  defineConfig({
    plugins: [solid()],
    test: {
      globals: true,
      environment: "jsdom",
      includeSource: [
        'src/**/*.{js,ts,tsx}'
      ],
      coverage: {
        provider: "istanbul",
        include: [
          'src/**/*.{js,ts,tsx}',
        ],
        exclude: [
          '**/entry-client.tsx',
          '**/src/entry-server.tsx',
        ],
      }
    },
    define: {
      'import.meta.vitest': 'undefined',
    },
    resolve: {
      conditions: ["development", "browser"],
    },
  }));
