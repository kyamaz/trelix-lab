import { vi } from 'vitest';
// Mock the import.meta.env object
vi.mock('import.meta', () => ({
  env: {
    VITE_API_DOMAIN: 'http://test-api.example.com',
    // Add any other environment variables you need
  },
}));
