import { ssr, ssrHydrationKey, escape, ssrAttribute } from 'solid-js/web';
import { M as Me, k } from '../runtime.mjs';
import { d as d$1 } from './createAsync-ChY9vPNL.mjs';
import 'node:http';
import 'node:https';
import 'node:zlib';
import 'node:stream';
import 'node:buffer';
import 'node:util';
import 'node:url';
import 'node:net';
import 'node:fs';
import 'node:path';
import 'fs';
import 'path';
import 'node:async_hooks';
import 'vinxi/lib/invariant';
import 'vinxi/lib/path';
import 'solid-js/web/storage';
import 'solid-js';
import 'solid-js/store';
import 'seroval';
import 'seroval-plugins/web';

var m = ["<main", ' class="text-center mx-auto text-gray-700 p-4">', "</main>"], p = ["<ul", "><li><a", ">", "</a></li></ul>"];
const u = Me(f, "c_9422", "$$function0"), d = k(u, "board");
function v() {
  var _a;
  const t = d$1(() => d());
  return ssr(m, ssrHydrationKey(), escape((_a = t()) == null ? void 0 : _a.map((a) => ssr(p, ssrHydrationKey(), ssrAttribute("href", escape("/board/".concat(a.id), true), false), escape(a.name)))));
}
async function f() {
  return fetch("http://trellix.kyz.one:3000/api/board").then((t) => t.json()).then((t) => {
    var _a;
    return (_a = t == null ? void 0 : t.data) != null ? _a : [];
  });
}

export { f as $$function0, v as default };
//# sourceMappingURL=index22.mjs.map
