import { ssr, ssrHydrationKey, ssrAttribute, escape } from 'solid-js/web';
import { U, M as Me, k } from '../runtime.mjs';
import { d } from './createAsync-ChY9vPNL.mjs';
import 'node:http';
import 'node:https';
import 'node:zlib';
import 'node:stream';
import 'node:buffer';
import 'node:util';
import 'node:url';
import 'node:net';
import 'node:fs';
import 'node:path';
import 'fs';
import 'path';
import 'node:async_hooks';
import 'vinxi/lib/invariant';
import 'vinxi/lib/path';
import 'solid-js/web/storage';
import 'solid-js';
import 'solid-js/store';
import 'seroval';
import 'seroval-plugins/web';

function f(t, e = 302) {
  let n, o;
  typeof e == "number" ? n = { status: e } : ({ revalidate: o, ...n } = e, typeof n.status > "u" && (n.status = 302));
  const r = new Headers(n.headers);
  return r.set("Location", t), o && r.set("X-Revalidate", o.toString()), new Response(null, { ...n, headers: r });
}
var b = ["<form", ' class="flex flex-col mx-auto w-56"', ' method="post"><input type="text" name="id"', ' hidden><input type="text" name="boardId"', ' hidden><label class="w-full inline-block pt-6" for="title">title:</label><input class="w-full border-gray-200 h-10 px-1 border rounded-md" type="text" name="title"', '><label class="w-full inline-block pt-6" for="content">content:</label><textarea class="w-full border-gray-200 px-1 border rounded-md" rows="3" name="content"', '></textarea><button class="mt-6 py-2 bg-purple-500 hover:bg-purple-600 text-white font-medium rounded-md " type="submit">Submit</button></form>'];
const h = U(Me($, "c_11330", "$$function0")), y = Me(w, "c_11330", "$$function1"), x = k((t) => y(t), "item");
function E({ params: t }) {
  var _a, _b, _c, _d;
  const e = d(() => x(t.item_id));
  return ssr(b, ssrHydrationKey(), ssrAttribute("action", escape(h, true), false), ssrAttribute("value", escape((_a = e()) == null ? void 0 : _a.id, true), false), ssrAttribute("value", escape((_b = e()) == null ? void 0 : _b.boardId, true), false), ssrAttribute("value", escape((_c = e()) == null ? void 0 : _c.title, true), false), ssrAttribute("value", escape((_d = e()) == null ? void 0 : _d.content, true), false));
}
async function $(t) {
  var _a;
  const e = t.get("title"), n = (_a = t.get("content")) != null ? _a : null, o = t.get("id"), r = t.get("boardId");
  if (!e || !o)
    return new Error("title and id is required");
  if (!(await fetch(`http://trellix.kyz.one:4000/api/item/${o}`, { method: "PUT", headers: { "Content-Type": "application/json" }, body: JSON.stringify({ title: e, content: n }) })).ok)
    return new Error("Failed to update item");
  throw f(`/board/${r}`);
}
async function w(t) {
  return fetch(`http://trellix.kyz.one:3000/api/item/${t}`).then((e) => e.json()).then((e) => {
    var _a;
    return (_a = e == null ? void 0 : e.data) != null ? _a : [];
  });
}

export { $ as $$function0, w as $$function1, E as default };
//# sourceMappingURL=_item_id_22.mjs.map
