async function o({ params: { board_id: n } }) {
  return fetch("http://trellix.kyz.one:4000/api/board/".concat(n, "/column")).then((t) => t.json());
}

export { o as GET };
//# sourceMappingURL=_board_id_2.mjs.map
