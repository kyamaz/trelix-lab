import { ssr, ssrHydrationKey, escape, createComponent } from 'solid-js/web';
import { w } from '../runtime.mjs';
import 'node:http';
import 'node:https';
import 'node:zlib';
import 'node:stream';
import 'node:buffer';
import 'node:util';
import 'node:url';
import 'node:net';
import 'node:fs';
import 'node:path';
import 'fs';
import 'path';
import 'node:async_hooks';
import 'vinxi/lib/invariant';
import 'vinxi/lib/path';
import 'solid-js/web/storage';
import 'solid-js';
import 'solid-js/store';
import 'seroval';
import 'seroval-plugins/web';

var a=["<main",' class="text-center mx-auto text-gray-700 p-4"><h1 class="max-6-xs text-6xl text-sky-700 font-thin uppercase my-16">Not Found</h1><p class="mt-8">Visit <a href="https://solidjs.com" target="_blank" class="text-sky-600 hover:underline">solidjs.com</a> to learn how to build Solid apps.</p><p class="my-4"><!--$-->',"<!--/--> - <!--$-->","<!--/--></p></main>"];function u(){return ssr(a,ssrHydrationKey(),escape(createComponent(w,{href:"/",class:"text-sky-600 hover:underline",children:"Home"})),escape(createComponent(w,{href:"/about",class:"text-sky-600 hover:underline",children:"About Page"})))}

export { u as default };
//# sourceMappingURL=_...404_.mjs.map
