import { createResource, untrack, sharedConfig } from 'solid-js';
import { isServer } from 'solid-js/web';

function d(n, r) {
  let e, a = () => !e || e.state === "unresolved" ? void 0 : e.latest;
  return [e] = createResource(() => w(n, untrack(a)), (c) => c, r), () => e();
}
class t {
  static all() {
    return new t();
  }
  static allSettled() {
    return new t();
  }
  static any() {
    return new t();
  }
  static race() {
    return new t();
  }
  static reject() {
    return new t();
  }
  static resolve() {
    return new t();
  }
  catch() {
    return new t();
  }
  then() {
    return new t();
  }
  finally() {
    return new t();
  }
}
function w(n, r) {
  if (isServer || !sharedConfig.context)
    return n(r);
  const e = fetch, a = Promise;
  try {
    return window.fetch = () => new t(), Promise = t, n(r);
  } finally {
    window.fetch = e, Promise = a;
  }
}

export { d };
//# sourceMappingURL=createAsync-ChY9vPNL.mjs.map
