import { createComponent, ssr, ssrHydrationKey, escape, Portal, ssrAttribute, ssrStyle } from 'solid-js/web';
import { U, M as Me$1, k as k$1, a as M, b as E } from '../runtime.mjs';
import { createContext, createSignal, mergeProps, Show, createEffect, untrack, For, useContext, onMount, onCleanup, batch } from 'solid-js';
import { createStore } from 'solid-js/store';
import { A } from './components-C8UB3ZmP.mjs';
import { d } from './createAsync-ChY9vPNL.mjs';
import 'node:http';
import 'node:https';
import 'node:zlib';
import 'node:stream';
import 'node:buffer';
import 'node:util';
import 'node:url';
import 'node:net';
import 'node:fs';
import 'node:path';
import 'fs';
import 'path';
import 'node:async_hooks';
import 'vinxi/lib/invariant';
import 'vinxi/lib/path';
import 'solid-js/web/storage';
import 'seroval';
import 'seroval-plugins/web';

var __defProp = Object.defineProperty;
var __defNormalProp = (obj, key, value) => key in obj ? __defProp(obj, key, { enumerable: true, configurable: true, writable: true, value }) : obj[key] = value;
var __publicField = (obj, key, value) => {
  __defNormalProp(obj, typeof key !== "symbol" ? key + "" : key, value);
  return value;
};
var Ae = ["<div", ' style="', '">', "</div>"], J = class {
  constructor(e) {
    __publicField(this, "x");
    __publicField(this, "y");
    __publicField(this, "width");
    __publicField(this, "height");
    this.x = Math.floor(e.x), this.y = Math.floor(e.y), this.width = Math.floor(e.width), this.height = Math.floor(e.height);
  }
  get rect() {
    return { x: this.x, y: this.y, width: this.width, height: this.height };
  }
  get left() {
    return this.x;
  }
  get top() {
    return this.y;
  }
  get right() {
    return this.x + this.width;
  }
  get bottom() {
    return this.y + this.height;
  }
  get center() {
    return { x: this.x + this.width * 0.5, y: this.y + this.height * 0.5 };
  }
  get corners() {
    return { topLeft: { x: this.left, y: this.top }, topRight: { x: this.right, y: this.top }, bottomRight: { x: this.left, y: this.bottom }, bottomLeft: { x: this.right, y: this.bottom } };
  }
}, G = (e) => {
  let r = new J(e.getBoundingClientRect());
  const { transform: t } = getComputedStyle(e);
  return t && (r = Ce(r, t)), r;
}, Ce = (e, r) => {
  let t, n;
  if (r.startsWith("matrix3d(")) {
    const s = r.slice(9, -1).split(/, /);
    t = +s[12], n = +s[13];
  } else if (r.startsWith("matrix(")) {
    const s = r.slice(7, -1).split(/, /);
    t = +s[4], n = +s[5];
  } else
    t = 0, n = 0;
  return new J({ ...e, x: e.x - t, y: e.y - n });
}, k = () => ({ x: 0, y: 0 }), oe = (e, r) => e.x === r.x && e.y === r.y, Q = (e, r) => new J({ ...e, x: e.x + r.x, y: e.y + r.y }), Y = (e, r) => Math.sqrt(Math.pow(e.x - r.x, 2) + Math.pow(e.y - r.y, 2)), Ee = (e, r) => {
  const t = Math.max(e.top, r.top), n = Math.max(e.left, r.left), s = Math.min(e.right, r.right), g = Math.min(e.bottom, r.bottom), p = s - n, u = g - t;
  if (n < s && t < g) {
    const y = e.width * e.height, i = r.width * r.height, l = p * u;
    return l / (y + i - l);
  }
  return 0;
}, Z = (e, r) => e.x === r.x && e.y === r.y && e.width === r.width && e.height === r.height, Me = (e, r, t) => {
  const n = e.transformed.corners, s = { distance: 1 / 0, droppable: null };
  for (const g of r) {
    const p = g.layout.corners, u = Y(p.topLeft, n.topLeft) + Y(p.topRight, n.topRight) + Y(p.bottomRight, n.bottomRight) + Y(p.bottomLeft, n.bottomLeft);
    u < s.distance ? (s.distance = u, s.droppable = g) : u === s.distance && g.id === t.activeDroppableId && (s.droppable = g);
  }
  return s.droppable;
}, ke = (e, r, t) => {
  const n = e.transformed, s = { ratio: 0, droppable: null };
  for (const g of r) {
    const p = Ee(n, g.layout);
    p > s.ratio ? (s.ratio = p, s.droppable = g) : p > 0 && p === s.ratio && g.id === t.activeDroppableId && (s.droppable = g);
  }
  return s.droppable;
}, ge = createContext(), Pe = (e) => {
  const r = mergeProps({ collisionDetector: ke }, e), [t, n] = createStore({ draggables: {}, droppables: {}, sensors: {}, active: { draggableId: null, get draggable() {
    return t.active.draggableId !== null ? t.draggables[t.active.draggableId] : null;
  }, droppableId: null, get droppable() {
    return t.active.droppableId !== null ? t.droppables[t.active.droppableId] : null;
  }, sensorId: null, get sensor() {
    return t.active.sensorId !== null ? t.sensors[t.active.sensorId] : null;
  }, overlay: null } }), s = (o, a, c) => {
    o.substring(0, o.length - 1), untrack(() => t[o][a]) && n(o, a, "transformers", c.id, c);
  }, g = (o, a, c) => {
    o.substring(0, o.length - 1), untrack(() => t[o][a]) && untrack(() => t[o][a].transformers[c]) && n(o, a, "transformers", c, void 0);
  }, p = ({ id: o, node: a, layout: c, data: $ }) => {
    const f = t.draggables[o], d = { id: o, node: a, layout: c, data: $, _pendingCleanup: false };
    let m;
    if (!f)
      Object.defineProperties(d, { transformers: { enumerable: true, configurable: true, writable: true, value: {} }, transform: { enumerable: true, configurable: true, get: () => {
        if (t.active.overlay)
          return k();
        const I = Object.values(t.draggables[o].transformers);
        return I.sort((M, B) => M.order - B.order), I.reduce((M, B) => B.callback(M), k());
      } }, transformed: { enumerable: true, configurable: true, get: () => Q(t.draggables[o].layout, t.draggables[o].transform) } });
    else if (t.active.draggableId === o && !t.active.overlay) {
      const I = { x: f.layout.x - c.x, y: f.layout.y - c.y }, M = "addDraggable-existing-offset", B = f.transformers[M], se = B ? B.callback(I) : I;
      m = { id: M, order: 100, callback: (ie) => ({ x: ie.x + se.x, y: ie.y + se.y }) }, V(() => g("draggables", o, M));
    }
    batch(() => {
      n("draggables", o, d), m && s("draggables", o, m);
    }), t.active.draggable && O();
  }, u = (o) => {
    untrack(() => t.draggables[o]) && (n("draggables", o, "_pendingCleanup", true), queueMicrotask(() => y(o)));
  }, y = (o) => {
    var _a;
    if ((_a = t.draggables[o]) == null ? void 0 : _a._pendingCleanup) {
      const a = t.active.draggableId === o;
      batch(() => {
        a && n("active", "draggableId", null), n("draggables", o, void 0);
      });
    }
  }, i = ({ id: o, node: a, layout: c, data: $ }) => {
    const f = t.droppables[o], d = { id: o, node: a, layout: c, data: $, _pendingCleanup: false };
    f || Object.defineProperties(d, { transformers: { enumerable: true, configurable: true, writable: true, value: {} }, transform: { enumerable: true, configurable: true, get: () => {
      const m = Object.values(t.droppables[o].transformers);
      return m.sort((I, M) => I.order - M.order), m.reduce((I, M) => M.callback(I), k());
    } }, transformed: { enumerable: true, configurable: true, get: () => Q(t.droppables[o].layout, t.droppables[o].transform) } }), n("droppables", o, d), t.active.draggable && O();
  }, l = (o) => {
    untrack(() => t.droppables[o]) && (n("droppables", o, "_pendingCleanup", true), queueMicrotask(() => v(o)));
  }, v = (o) => {
    var _a;
    if ((_a = t.droppables[o]) == null ? void 0 : _a._pendingCleanup) {
      const a = t.active.droppableId === o;
      batch(() => {
        a && n("active", "droppableId", null), n("droppables", o, void 0);
      });
    }
  }, h = ({ id: o, activators: a }) => {
    n("sensors", o, { id: o, activators: a, coordinates: { origin: { x: 0, y: 0 }, current: { x: 0, y: 0 }, get delta() {
      return { x: t.sensors[o].coordinates.current.x - t.sensors[o].coordinates.origin.x, y: t.sensors[o].coordinates.current.y - t.sensors[o].coordinates.origin.y };
    } } });
  }, w = (o) => {
    if (!untrack(() => t.sensors[o]))
      return;
    const a = t.active.sensorId === o;
    batch(() => {
      a && n("active", "sensorId", null), n("sensors", o, void 0);
    });
  }, x = ({ node: o, layout: a }) => {
    const c = t.active.overlay, $ = { node: o, layout: a };
    c || Object.defineProperties($, { id: { enumerable: true, configurable: true, get: () => {
      var _a;
      return (_a = t.active.draggable) == null ? void 0 : _a.id;
    } }, data: { enumerable: true, configurable: true, get: () => {
      var _a;
      return (_a = t.active.draggable) == null ? void 0 : _a.data;
    } }, transformers: { enumerable: true, configurable: true, get: () => Object.fromEntries(Object.entries(t.active.draggable ? t.active.draggable.transformers : {}).filter(([f]) => f !== "addDraggable-existing-offset")) }, transform: { enumerable: true, configurable: true, get: () => {
      const f = Object.values(t.active.overlay ? t.active.overlay.transformers : []);
      return f.sort((d, m) => d.order - m.order), f.reduce((d, m) => m.callback(d), k());
    } }, transformed: { enumerable: true, configurable: true, get: () => t.active.overlay ? Q(t.active.overlay.layout, t.active.overlay.transform) : new J({ x: 0, y: 0, width: 0, height: 0 }) } }), n("active", "overlay", $);
  }, C = () => n("active", "overlay", null), b = (o, a) => {
    batch(() => {
      n("sensors", o, "coordinates", { origin: { ...a }, current: { ...a } }), n("active", "sensorId", o);
    });
  }, E = (o) => {
    const a = t.active.sensorId;
    a && n("sensors", a, "coordinates", "current", { ...o });
  }, j = () => n("active", "sensorId", null), T = (o, a) => {
    var _a;
    const c = {};
    for (const f of Object.values(t.sensors))
      if (f)
        for (const [d, m] of Object.entries(f.activators))
          (_a = c[d]) != null ? _a : c[d] = [], c[d].push({ sensor: f, activator: m });
    const $ = {};
    for (const f in c) {
      let d = f;
      a && (d = `on${f}`), $[d] = (m) => {
        for (const { activator: I } of c[f]) {
          if (t.active.sensor)
            break;
          I(m, o);
        }
      };
    }
    return $;
  }, O = () => {
    let o = false;
    const a = Object.values(t.draggables), c = Object.values(t.droppables), $ = t.active.overlay;
    return batch(() => {
      const f = /* @__PURE__ */ new WeakMap();
      for (const d of a)
        if (d) {
          const m = d.layout;
          f.has(d.node) || f.set(d.node, G(d.node));
          const I = f.get(d.node);
          Z(m, I) || (n("draggables", d.id, "layout", I), o = true);
        }
      for (const d of c)
        if (d) {
          const m = d.layout;
          f.has(d.node) || f.set(d.node, G(d.node));
          const I = f.get(d.node);
          Z(m, I) || (n("droppables", d.id, "layout", I), o = true);
        }
      if ($) {
        const d = $.layout, m = G($.node);
        Z(d, m) || (n("active", "overlay", "layout", m), o = true);
      }
    }), o;
  }, S = () => {
    var _a;
    const o = (_a = t.active.overlay) != null ? _a : t.active.draggable;
    if (o) {
      const a = r.collisionDetector(o, Object.values(t.droppables), { activeDroppableId: t.active.droppableId }), c = a ? a.id : null;
      t.active.droppableId !== c && n("active", "droppableId", c);
    }
  }, R = (o) => {
    const a = { id: "sensorMove", order: 0, callback: (c) => t.active.sensor ? { x: c.x + t.active.sensor.coordinates.delta.x, y: c.y + t.active.sensor.coordinates.delta.y } : c };
    O(), batch(() => {
      n("active", "draggableId", o), s("draggables", o, a);
    }), S();
  }, X = () => {
    const o = untrack(() => t.active.draggableId);
    batch(() => {
      o !== null && g("draggables", o, "sensorMove"), n("active", ["draggableId", "droppableId"], null);
    }), O();
  }, ne = (o) => {
    createEffect(() => {
      const a = t.active.draggable;
      a && untrack(() => o({ draggable: a }));
    });
  }, U = (o) => {
    createEffect(() => {
      const a = t.active.draggable;
      if (a) {
        const c = untrack(() => t.active.overlay);
        Object.values(c ? c.transform : a.transform), untrack(() => o({ draggable: a, overlay: c }));
      }
    });
  }, ae = (o) => {
    createEffect(() => {
      const a = t.active.draggable, c = t.active.droppable;
      a && untrack(() => o({ draggable: a, droppable: c, overlay: t.active.overlay }));
    });
  }, V = (o) => {
    createEffect(({ previousDraggable: a, previousDroppable: c, previousOverlay: $ }) => {
      const f = t.active.draggable, d = f ? t.active.droppable : null, m = f ? t.active.overlay : null;
      return !f && a && untrack(() => o({ draggable: a, droppable: c, overlay: $ })), { previousDraggable: f, previousDroppable: d, previousOverlay: m };
    }, { previousDraggable: null, previousDroppable: null, previousOverlay: null });
  };
  U(() => S()), r.onDragStart && ne(r.onDragStart), r.onDragMove && U(r.onDragMove), r.onDragOver && ae(r.onDragOver), r.onDragEnd && V(r.onDragEnd);
  const me = [t, { addTransformer: s, removeTransformer: g, addDraggable: p, removeDraggable: u, addDroppable: i, removeDroppable: l, addSensor: h, removeSensor: w, setOverlay: x, clearOverlay: C, recomputeLayouts: O, detectCollisions: S, draggableActivators: T, sensorStart: b, sensorMove: E, sensorEnd: j, dragStart: R, dragEnd: X, onDragStart: ne, onDragMove: U, onDragOver: ae, onDragEnd: V }];
  return createComponent(ge.Provider, { value: me, get children() {
    return r.children;
  } });
}, z = () => useContext(ge) || null, je = (e = "pointer-sensor") => {
  const [r, { addSensor: t, removeSensor: n, sensorStart: s, sensorMove: g, sensorEnd: p, dragStart: u, dragEnd: y }] = z(), i = 250, l = 10;
  onMount(() => {
    t({ id: e, activators: { pointerdown: C } });
  }), onCleanup(() => {
    n(e);
  });
  const v = () => r.active.sensorId === e, h = { x: 0, y: 0 };
  let w = null, x = null;
  const C = (S, R) => {
    S.button === 0 && (document.addEventListener("pointermove", j), document.addEventListener("pointerup", T), x = R, h.x = S.clientX, h.y = S.clientY, w = window.setTimeout(E, i));
  }, b = () => {
    w && (clearTimeout(w), w = null), document.removeEventListener("pointermove", j), document.removeEventListener("pointerup", T), document.removeEventListener("selectionchange", O);
  }, E = () => {
    r.active.sensor ? v() || b() : (s(e, h), u(x), O(), document.addEventListener("selectionchange", O));
  }, j = (S) => {
    const R = { x: S.clientX, y: S.clientY };
    if (!r.active.sensor) {
      const X = { x: R.x - h.x, y: R.y - h.y };
      Math.sqrt(X.x ** 2 + X.y ** 2) > l && E();
    }
    v() && (S.preventDefault(), g(R));
  }, T = (S) => {
    b(), v() && (S.preventDefault(), y(), p());
  }, O = () => {
    var _a;
    (_a = window.getSelection()) == null ? void 0 : _a.removeAllRanges();
  };
}, Te = (e) => (je(), e.children), K = (e) => ({ transform: `translate3d(${e.x}px, ${e.y}px, 0)` }), Le = (e, r = {}) => {
  const [t, { addDraggable: n, removeDraggable: s, draggableActivators: g }] = z(), [p, u] = createSignal(null);
  onMount(() => {
    const v = p();
    v && n({ id: e, node: v, layout: G(v), data: r });
  }), onCleanup(() => s(e));
  const y = () => t.active.draggableId === e, i = () => {
    var _a;
    return ((_a = t.draggables[e]) == null ? void 0 : _a.transform) || k();
  };
  return Object.defineProperties((v, h) => {
    const w = h ? h() : {};
    createEffect(() => {
      const x = p(), C = g(e);
      if (x)
        for (const b in C)
          x.addEventListener(b, C[b]);
      onCleanup(() => {
        if (x)
          for (const b in C)
            x.removeEventListener(b, C[b]);
      });
    }), u(v), w.skipTransform || createEffect(() => {
      var _a;
      const x = i();
      if (oe(x, k()))
        v.style.removeProperty("transform");
      else {
        const C = K(i());
        v.style.setProperty("transform", (_a = C.transform) != null ? _a : null);
      }
    });
  }, { ref: { enumerable: true, value: u }, isActiveDraggable: { enumerable: true, get: y }, dragActivators: { enumerable: true, get: () => g(e, true) }, transform: { enumerable: true, get: i } });
}, Re = (e, r = {}) => {
  const [t, { addDroppable: n, removeDroppable: s }] = z(), [g, p] = createSignal(null);
  onMount(() => {
    const l = g();
    l && n({ id: e, node: l, layout: G(l), data: r });
  }), onCleanup(() => s(e));
  const u = () => t.active.droppableId === e, y = () => {
    var _a;
    return ((_a = t.droppables[e]) == null ? void 0 : _a.transform) || k();
  };
  return Object.defineProperties((l, v) => {
    const h = v ? v() : {};
    p(l), h.skipTransform || createEffect(() => {
      var _a;
      const w = y();
      if (oe(w, k()))
        l.style.removeProperty("transform");
      else {
        const x = K(y());
        l.style.setProperty("transform", (_a = x.transform) != null ? _a : null);
      }
    });
  }, { ref: { enumerable: true, value: p }, isActiveDroppable: { enumerable: true, get: u }, transform: { enumerable: true, get: y } });
}, qe = (e) => {
  const [r, { onDragStart: t, onDragEnd: n, setOverlay: s, clearOverlay: g }] = z();
  t(({ draggable: u }) => {
    s({ node: u.node, layout: u.layout }), queueMicrotask(() => {
    });
  }), n(() => queueMicrotask(g));
  const p = () => {
    const u = r.active.overlay, y = r.active.draggable;
    return !u || !y ? {} : { position: "fixed", transition: "transform 0s", top: `${u.layout.top}px`, left: `${u.layout.left}px`, "min-width": `${y.layout.width}px`, "min-height": `${y.layout.height}px`, ...K(u.transform), ...e.style };
  };
  return createComponent(Portal, { get mount() {
    return document.body;
  }, get children() {
    return createComponent(Show, { get when() {
      return r.active.draggable;
    }, get children() {
      return ssr(Ae, ssrHydrationKey() + ssrAttribute("class", escape(e.class, true), false), ssrStyle(p()), typeof e.children == "function" ? escape(e.children(r.active.draggable)) : escape(e.children));
    } });
  } });
}, Ne = (e, r, t) => {
  const n = e.slice();
  return n.splice(t, 0, ...n.splice(r, 1)), n;
}, pe = createContext(), Be = (e) => {
  const [r] = z(), [t, n] = createStore({ initialIds: [], sortedIds: [] }), s = (u) => u >= 0 && u < t.initialIds.length;
  createEffect(() => {
    n("initialIds", [...e.ids]), n("sortedIds", [...e.ids]);
  }), createEffect(() => {
    r.active.draggableId && r.active.droppableId ? untrack(() => {
      const u = t.sortedIds.indexOf(r.active.draggableId), y = t.initialIds.indexOf(r.active.droppableId);
      if (!s(u) || !s(y))
        n("sortedIds", [...e.ids]);
      else if (u !== y) {
        const i = Ne(t.sortedIds, u, y);
        n("sortedIds", i);
      }
    }) : n("sortedIds", [...e.ids]);
  });
  const p = [t, {}];
  return createComponent(pe.Provider, { value: p, get children() {
    return e.children;
  } });
}, ze = () => useContext(pe) || null, Ge = (e, r) => (t) => {
  e(t), r(t);
}, fe = (e, r = {}) => {
  const [t, { addTransformer: n, removeTransformer: s }] = z(), [g] = ze(), p = Le(e, r), u = Re(e, r), y = Ge(p.ref, u.ref), i = () => g.initialIds.indexOf(e), l = () => g.sortedIds.indexOf(e), v = (b) => {
    var _a;
    return ((_a = t.droppables[b]) == null ? void 0 : _a.layout) || null;
  }, h = () => {
    const b = k(), E = i(), j = l();
    if (j !== E) {
      const T = v(e), O = v(g.initialIds[j]);
      T && O && (b.x = O.x - T.x, b.y = O.y - T.y);
    }
    return b;
  }, w = { id: "sortableOffset", order: 100, callback: (b) => {
    const E = h();
    return { x: b.x + E.x, y: b.y + E.y };
  } };
  onMount(() => n("droppables", e, w)), onCleanup(() => s("droppables", e, w.id));
  const x = () => {
    var _a, _b;
    return (e === t.active.draggableId && !t.active.overlay ? (_a = t.draggables[e]) == null ? void 0 : _a.transform : (_b = t.droppables[e]) == null ? void 0 : _b.transform) || k();
  };
  return Object.defineProperties((b) => {
    p(b, () => ({ skipTransform: true })), u(b, () => ({ skipTransform: true })), createEffect(() => {
      var _a;
      const E = x();
      if (oe(E, k()))
        b.style.removeProperty("transform");
      else {
        const j = K(x());
        b.style.setProperty("transform", (_a = j.transform) != null ? _a : null);
      }
    });
  }, { ref: { enumerable: true, value: y }, transform: { enumerable: true, get: x }, isActiveDraggable: { enumerable: true, get: () => p.isActiveDraggable }, dragActivators: { enumerable: true, get: () => p.dragActivators }, isActiveDroppable: { enumerable: true, get: () => u.isActiveDroppable } });
}, We = ["<button", ' type="button">', "</button>"];
const le = U(Me$1(Ye, "c_8385", "$$function0"));
function Xe({ id: e }) {
  M(le), console.log("I'm fine grained. should not be logged while  action is pending");
  const r = E(le, (t) => t.some((n) => n === e));
  return ssr(We, ssrHydrationKey(), r.pending ? "deleting" : "delete");
}
async function Ye(e) {
  await new Promise((r) => setTimeout(r, 4e3)), await fetch(`http://trellix.kyz.one:4000/api/item/${e.toString()}`, { method: "DELETE" });
}
var Fe = ["<div", ' class="h-24 bg-white rounded-md shadow-2xl"><h3 class="font-medium">', "</h3></div>"], He = ["<li", '><div class="py-2 px-4 bg-white rounded-md min-h-24"><div class="flex gap-x-4 justify-end"><!--$-->', "<!--/--><!--$-->", '<!--/--></div><h3 class="font-medium">', "</h3><p>", "</p></div></li>"];
const ve = (e) => ssr(Fe, ssrHydrationKey(), escape(e.title));
function Je({ item: e }) {
  return fe(e.id, { type: "item", col_id: e.column_id, position: e.position, title: e.title }), ssr(He, ssrHydrationKey(), escape(createComponent(Xe, { get id() {
    return e.id;
  } })), escape(createComponent(A, { get href() {
    return `/board/${e.column_id}/item/${e.id}`;
  }, children: "edit " })), escape(e.title), escape(e.content));
}
var Ke = ["<div", '><div class="column-header">', '</div><div class="column bg-gray-100">', "</div></div>"], Ue = ["<ul", ' class="flex flex-1 gap-4 overflow-auto flex-col bg-gray-200 rounded-lg py-2"><li class="pt-2 px-4 flex justify-between"><h2 class="font-semibold"> <!--$-->', '<!--/--> </h2><button type="button"> new card</button></li><li><ul class="px-4 grid gap-4">', "</ul></li></ul>"];
function be(e) {
  return e + 1024;
}
function Ve(e) {
  return e - 1024;
}
const Qe = U(Me$1(tt, "c_7624", "$$function0")), Ze = (e) => ssr(Ke, ssrHydrationKey(), escape(e.title), escape(createComponent(For, { get each() {
  return e.items;
}, children: (r) => createComponent(ve, { get title() {
  return r.title;
} }) })));
function et({ col: e }) {
  return fe(be(e.id), { type: "group" }), M(Qe), ssr(Ue, ssrHydrationKey(), escape(e.name), escape(e.items.map((r) => createComponent(Je, { item: r }))));
}
async function tt(e) {
  await fetch("http://trellix.kyz.one:4000/api/item", { method: "POST", headers: { "Content-Type": "application/json" }, body: JSON.stringify({ title: "new task", content: null, columnId: e }) });
}
var rt = ["<main", ' class="grid grid-cols-3 overflow-auto gap-6 w-full mx-auto h-[calc(100vh-50px)] px-6 pt-8 pb-16">', "</main>"];
const ot = U(Me$1(at, "c_7587", "$$function0"));
function nt({ columns: e }) {
  const r = M(ot), [t, n] = createSignal("before"), s = () => {
    var _a;
    return (_a = e()) == null ? void 0 : _a.map((i) => be(i.id));
  }, g = (i) => i.data.type === "group";
  console.log("where am i component ?");
  function p(i, l) {
    if (!i || !l)
      return;
    if (i.data.type === "group" && l.data.type === "group") {
      console.warn("cant move group to group.weird colision handeling");
      return;
    }
    if (i.id === l.id && l.data.type === "item")
      return;
    const v = i.id === l.id || l.data.type === "group" ? { id: void 0, col_id: Ve(l.id), position: void 0 } : { id: l.id, col_id: l.data.col_id, position: l.data.position }, h = { from: { id: i.id, position: i.data.position, column_id: i.data.col_id }, to: { id: v.id, position: v.position, column_id: v.col_id }, insert: t() };
    r(h);
  }
  return createComponent(Pe, { collisionDetector: (i, l, v) => {
    const h = Me(i, l, v);
    if ((h == null ? void 0 : h.data.type) === "group")
      return h;
    const w = i.transformed.y > h.transformed.y ? "after" : "before";
    return n(w), h;
  }, onDragEnd: ({ draggable: i, droppable: l }) => {
    p(i, l);
  }, get children() {
    return [createComponent(Te, {}), ssr(rt, ssrHydrationKey(), escape(createComponent(Be, { get ids() {
      return s();
    }, get children() {
      var _a;
      return (_a = e()) == null ? void 0 : _a.map((i) => createComponent(et, { col: i }));
    } }))), createComponent(qe, { children: (i) => {
      if (!i)
        return null;
      const l = i.data;
      return g(i) ? createComponent(Ze, { get title() {
        return l.title;
      }, items: [] }) : createComponent(ve, { get title() {
        return l.title;
      } });
    } })];
  } });
}
async function at(e) {
  console.log("where am i action?"), await fetch("http://trellix.kyz.one:4000/api/item/move", { method: "PATCH", headers: { "Content-Type": "application/json" }, body: JSON.stringify(e) });
}
const st = Me$1(lt, "c_10473", "$$function0"), it = k$1((e) => st(e), "board");
function Dt({ params: e }) {
  console.debug("where are you  ?");
  const r = d(() => it(e.board_id));
  return createComponent(nt, { columns: r });
}
async function lt(e) {
  try {
    return fetch(`http://trellix.kyz.one:3000/api/board/${e}`).then((r) => r.json()).then((r) => {
      var _a;
      return (_a = r == null ? void 0 : r.data) != null ? _a : [];
    });
  } catch (r) {
    return console.error(r), [];
  }
}

export { lt as $$function0, Dt as default };
//# sourceMappingURL=index4.mjs.map
