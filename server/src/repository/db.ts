import { Database } from "bun:sqlite";

const createAccountQuery = `
CREATE TABLE IF NOT EXISTS "account" (
    "id" INTEGER PRIMARY KEY AUTOINCREMENT,
    "email" TEXT UNIQUE,
    "password" TEXT NOT NULL UNIQUE
);
`;
const createBoardQuery = `
CREATE TABLE IF NOT EXISTS "board" (
    "id" INTEGER PRIMARY KEY AUTOINCREMENT,
    "name" TEXT,
    "color" TEXT DEFAULT '#e0e0e0',
    "createdAt" DATETIME DEFAULT CURRENT_TIMESTAMP,
    "accountId" INTEGER,
    FOREIGN KEY("accountId") REFERENCES "Account"("id")
);
`;
const createColQuery = `
CREATE TABLE IF NOT EXISTS "column" (
    "id" INTEGER PRIMARY KEY AUTOINCREMENT,
    "name" TEXT,
    "position" REAL DEFAULT 0,
    "boardId" INTEGER,
    FOREIGN KEY("boardId") REFERENCES "Board"("id") ON DELETE CASCADE
    UNIQUE("position", "boardId") 
);
`;
const createItemQuery = `
CREATE TABLE IF NOT EXISTS "item" (
    "id" INTEGER PRIMARY KEY AUTOINCREMENT,
    "title" TEXT,
    "content" TEXT,
    "completed" BOOLEAN DEFAULT FALSE,
    "dueDate" DATETIME  DEFAULT NULL,
    "position" REAL,
    "columnId" INTEGER,
    FOREIGN KEY("columnId") REFERENCES "Column"("id")
);
`;
function seed_account() {
	const q = db.query(`
    INSERT INTO "account" ("email","password") 
    VALUES ('test@test.fr', '1234')`);
	q.run();
}
function seed_board() {
	const q = db.query(`
    INSERT INTO "board" ("name", "color", "accountId")
    VALUES ('First Board', '#e0e0e0', 1),
           ('Second Board', '#d0d0d0', 1);`);
	q.run();
}

function seed_column() {
	const q = db.query(`
    INSERT INTO "column" ("name", "position", "boardId")
    VALUES ('To Do', 0, 1),
           ('In Progress', 1, 1),
           ('Done', 2, 1),
           ('Backlog', 0, 2);`);
	q.run();
}

function seed_item() {
	const pos_buffer = 1024;

	for (let i = 1; i < 30; i++) {
		const  position= i*pos_buffer;
		const q = db.query(`
		INSERT INTO "item" ("title", "content", "columnId", "position", "completed", "dueDate")
		VALUES ('Task ${i} inside  col 1', 'This is the a task.',1 , ${position},  FALSE, NULL);
		`);
		q.run();
	}
	for (let i = 1; i < 4; i++) {
		const q = db.query(`
		INSERT INTO "item" ("title", "content", "columnId", "position", "completed", "dueDate")
		VALUES ('Task ${30+i}', 'This is the a task.',2 , ${pos_buffer}*${i},  FALSE, NULL);
		`);
		q.run();
	}
		const q = db.query(`
		INSERT INTO "item" ("title", "content", "columnId", "position", "completed", "dueDate")
		VALUES ('Task 35', 'This is the a task.',3 , ${pos_buffer},  TRUE, NULL);
		`);
		q.run();
}

const db = new Database("trellix.db", { create: true });
function seed() {
	let query = db.query(createAccountQuery);
	query.run();
	query = db.query(createBoardQuery);
	query.run();
	query = db.query(createColQuery);
	query.run();
	query = db.query(createItemQuery);
	query.run();

	seed_account();
	seed_board();
	seed_column();
	seed_item();
	console.info("db seeded");
}
async function create_data() {
	const path = "./trellix.db";

	const file = Bun.file(path);

	if (!file.size) {
		seed();
	}
}

export { db, create_data };
