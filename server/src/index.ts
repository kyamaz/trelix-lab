import { Hono } from "hono";
import { board_controller } from "./routes/board";
import { column_controller } from "./routes/column";
import { create_data } from "./repository/db";
import { item_controller } from "./routes/item";
import { cors } from "hono/cors";
import { query_controller } from "./routes/query";
import { jarvis_controller } from "./routes/jarvis";
import { health_controller } from "./routes/health";
import { angular_controller } from "./routes/angular";
const app = new Hono().basePath("/api");

// setup db && seed data
create_data();

app.use(
	"*",
	cors({
		origin: ["http://localhost:3000", "http://localhost:5173"],
	}),
);
// sanity check

app.route("/health", health_controller);
app.route("/jarvis", jarvis_controller);

//kanban apis
app.route("/board", board_controller);
app.route("/board/:boardId/column", column_controller);
app.route("/item", item_controller);
//jarvis apis
app.route("/query", query_controller);
app.route("/angular", angular_controller);


console.log("server is running on port 4000");
export default {
	port: 4000,
	fetch: app.fetch,
};
