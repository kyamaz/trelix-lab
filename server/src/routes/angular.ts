import { Hono } from "hono";
import { JARVIS_URL } from "../shared";

const angular_controller = new Hono();

angular_controller.post("/query", async (c) => {
	try {
		const body = await c.req.json();
		return fetch(`${JARVIS_URL}about/query/stream`, {
			method: 'POST',
			body: JSON.stringify(body),
			headers: {
				'Content-Type': 'application/json'
			}
		})
	} catch (e: any) {
		console.log(e);
		return c.json({ msg: e.message });
	}
});

angular_controller.get("/source", async (c) => {
	try {
		return fetch(`${JARVIS_URL}about/embed/source`, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json'
			}
		})
	} catch (e: any) {
		console.log(e);
		return c.json({ msg: e.message });
	}
});
export { angular_controller };
