import { Hono } from "hono";
const health_controller = new Hono();
//health
health_controller.get("/", (c) => c.json({ msg: "proxy is up and running !" }));

health_controller.post("/", async (c) => {
	return c.req.json().then((res) => {
		res._msg = "bff: I got your request";
		return c.json(res);
	});
});
export { health_controller };
