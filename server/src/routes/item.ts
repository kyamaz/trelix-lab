import { Hono } from "hono";
import { db } from "../repository/db";
import { HTTPException } from "hono/http-exception";

const item_controller = new Hono();

//detail
const buffer_pos = 1024;

item_controller.get("/:item_id", (c) => {
	const { item_id } = c.req.param() as {
		item_id: string;
	};
	const data = db
		.query(`
				SELECT a.id, a.title, a.content, a.position, a.columnId, b.boardId
				FROM item as a 
				INNER JOIN column as b ON b.id = a.columnId
				WHERE a.id = $itemId;`)
		.get({
			$itemId: Number.parseInt(item_id),
		});
	return c.json({ data });
});

// create
item_controller.post("/", async (c) => {
	return c.req.json().then((payload) => {
		const { title, content, columnId } = payload;

		const max = db.query(`SELECT MAX(position) 
														FROM item
														WHERE columnId = $columnId 
	`).get({ $columnId: columnId }) as { "MAX(position)": number };

		const data = db
			.query(`
				INSERT INTO "item" ("title", "content", "columnId", "position") 
				VALUES (	$title, $content, $columnId, $position)
				RETURNING id;`)
			.get({
				$title: title,
				$content: content,
				$columnId: columnId,
				$position: max["MAX(position)"] + buffer_pos,
			});

		return c.json({ data });
	});
});
//update
item_controller.put("/:item_id", async (c) => {
	const id = c.req.param("item_id");
	return c.req.json().then((payload) => {
		const { title, content } = payload;
		const data = db
			.query(`
				UPDATE "item" 
				SET title=$title, content=$content
				WHERE id = $itemId
				RETURNING id, title, content
			`)
			.get({
				$itemId: id,
				$title: title,
				$content: content,
			});
		return c.json({ data });
	});
});

item_controller.delete("/:item_id", async (c) => {
	const id = c.req.param("item_id");
	await new Promise(resolve => setTimeout(resolve, 2000));
	console.log('delete item', id);

	const data = db
		.query(`
			DELETE FROM "item" 
			WHERE id = $id
			RETURNING id`)
		.get({
			$id: id,
		});

	return c.json({ data });
});

function swap_item(payload) {
	const { from, to } = payload;

	if (isNaN(from.position) || isNaN(to.position)) {
		//TODO  query  db  to get position
		console.error('position is not number  in from or to', from.position, to.position);;
		return null;
	}

	const updateposition = db.query(
		`UPDATE "item" 
			SET position = $toposition, columnId = $tocolumnId
			WHERE item.id = $fromid;`,
	);
	const swpapPosition = db.query(
		`UPDATE "item" 
			 SET position = $fromPosition, columnId = $toColumnId
			 WHERE item.id = $toId;`,
	);

	const swapTransaction = db.transaction((from, to) => {
		updateposition.run({
			$fromid: from.id,
			$toposition: to.position,
			$tocolumnId: to.column_id,
		});
		swpapPosition.run({
			$fromPosition: from.position,
			$toId: to.id,
			$toColumnId: from.column_id,
		});
	});

	try {
		swapTransaction(from, to);
		return "success";
	} catch (error) {
		console.error(error);
		return null;
	}
}

function get_adjacent(id: number, columnId: number) {
	return db
		.query(`
					WITH target_position AS (
						SELECT position, id, "pivot" FROM item WHERE id = $itemId AND columnId = $colId
					),
					items_before AS (
							SELECT position, id, "before"
							FROM item 
							WHERE columnId = $colId AND position < (SELECT position FROM target_position) 
							ORDER BY position DESC LIMIT 1
					),
					items_after AS (
							SELECT position, id, "after"
							FROM item 
							WHERE columnId = $colId AND position > (SELECT position FROM target_position) 
							ORDER BY position ASC LIMIT 1
					)

					SELECT * FROM target_position
					UNION ALL
					SELECT * FROM items_before
					UNION ALL
					SELECT * FROM items_after;
			`)
		.all({
			$itemId: id,
			$colId: columnId,
		}) as {
			position: number;
			id: number;
			pivot: "pivot" | "before" | "after";
		}[];
}
function get_first(columnId: number): { position?: number } {
	return db
		.query(`
					SELECT position 
					FROM item 
					WHERE columnId = $colId 
					ORDER BY position 
					ASC LIMIT 1;
			`)
		.get({
			$colId: columnId,
		}) as { position?: number };
}
function get_last(columnId: number): { position?: number } {
	return db
		.query(`
					SELECT position 
					FROM item
					WHERE columnId = $colId 
					ORDER BY position 
					DESC LIMIT 1;
		`)
		.get({
			$colId: columnId,
		}) as { position?: number };
}

function calc_new_position(
	values: {
		position: number;
		id: number;
		pivot: "pivot" | "before" | "after";
	}[],
	insert: "before" | "after",
	colId: number,
) {
	const pivot = values.find((v) => v.pivot === "pivot");

	if (!pivot) {
		return [null, "pivot not found"];
	}

	if (insert === "before") {
		const before = values.find((v) => v.pivot === "before");
		if (!before) {
			const _first = get_first(colId)?.position;
			//get  first value,  if  no value, then start with buffer
			const first = _first ? _first - _first / 2 : buffer_pos;
			return [first, null];
		}

		const deviation = pivot.position - before.position;
		const pos = before.position + deviation / 2;

		if (pos < before.position) {
			return [null, `expected: ${pos} to be greater ${before.position}`];
		}
		return [pos, null];
	}

	if (insert === "after") {
		const after = values.find((v) => v.pivot === "after");
		if (!after) {
			const _last = get_last(colId)?.position;
			const last = _last ? _last + buffer_pos / 2 : buffer_pos;
			return [last, null];
		}

		const deviation = after.position - pivot.position;
		const pos = after.position - deviation / 2;

		if (pos > after.position) {
			return [null, `expected: ${pos} to be less ${after.position}`];
		}
		return [pos, null];
	}

	return [null, `expected: pivot, before, after. got: ${insert}`];
}
function move_item({
	from,
	to,
	insert,
}: {
	from: { id: number; column_id: number };
	to: { id: number; column_id: number };
	insert: "before" | "after";
}) {
	const adjacent = get_adjacent(to.id, to.column_id);

	const [position, error] = calc_new_position(adjacent, insert, to.column_id);

	if (error) {
		return null;
	}

	console.debug(adjacent, position);

	const updatePosition = db.query(`
		UPDATE "item"  
		SET position = $position, columnId = $columnId
		WHERE item.id = $id;`);

	updatePosition.run({
		$id: from.id,
		$position: position,
		$columnId: to.column_id,
	});

	return "success";
}

item_controller.patch("/move", async (c) => {
	return c.req.json().then((payload) => {
		const { from, to } = payload;

		if (!to.id) {
			// destination  col is empty, insert item
			const updateposition = db.query(
				`	UPDATE "item"  
					SET position = $position, columnId = $columnId
					WHERE item.id = $id;`,
			);

			updateposition.run({
				$id: from.id,
				$position: buffer_pos * to.column_id,
				$columnId: to.column_id,
			});
			return c.json({ data: "success" });
		}

		if (to.column_id === from.column_id) {
			// same column, swap element as we know that there is a source and a target card
			// could use move but swap avoid recaculate position
			const swaped = swap_item({ from, to });
			if (!swaped) {
				throw new HTTPException(500, { message: "failed swap item" });
			}

			return c.json({ data: "success" });
		}

		// move item, need to recalculate position
		const moved = move_item(payload);
		if (!moved) {
			throw new HTTPException(500, { message: "failed move item" });
		}

		return c.json({ data: "success" });
	});
});
export { item_controller };
