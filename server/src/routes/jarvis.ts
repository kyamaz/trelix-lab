import { Hono } from "hono";
import { JARVIS_URL } from "../shared";

const jarvis_controller = new Hono();

//simple routes  to  check communication with jarvis
jarvis_controller.get("/", async (c) => {
	try {
		return fetch(`${JARVIS_URL}health`)
			.then((res) => res.json())
			.then((res) => {
				res._msg = "from  bff";
				return c.json(res);
			});
	} catch (e) {
		return c.json({ msg: "oups" });
	}
});

jarvis_controller.post("/", async (c) => {
	try {
		const body = await c.req.json();
		return fetch(`${JARVIS_URL}health`, {
			method: 'POST',
			body: JSON.stringify(body),
			headers: {
				'Content-Type': 'application/json'
			}
		})
			.then((res) => res.json())
			.then((res) => {
				return c.json(res);
			});
	} catch (e) {
		return c.json({ msg: "oups" });
	}
});

export { jarvis_controller };
