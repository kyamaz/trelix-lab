import { Hono } from "hono";
import { db } from "../repository/db";
const query_controller = new Hono();

// take a sql queyry to  execute from  jarvis
query_controller.post("/", async (c) => {
	try {
		const body = await c.req.json();
		const json = JSON.parse(body);
		const response = db.query(json.query).all();
		return c.json({ data: response });
	} catch (e: any) {
		console.log(e);
		return c.json({ msg: e.message });
	}
});

query_controller.get("/schema", async (c) => {
	try {

		const tables = db.query("SELECT name FROM sqlite_master WHERE type='table';")
			.all()
			.map((table) => table.name);
		const _t_names = tables.map((table) => `'${table}'`).join(',');
		const q = db.query(`
			SELECT tbl_name, sql 
			FROM sqlite_master 
			WHERE sql is NOT NULL AND tbl_name IN (${_t_names});`)
		const _schema = q.all({
			$tables: _t_names//TODO  does not work IDK
		});

		const schema = _schema.map((sc) => sc.sql).join('\n');
		const tables_names = tables.map((table) => `'${table}'`).join('\n');
		return c.json({
			data: {
				schema,
				tables_names: tables
			}
		});

	} catch (e: any) {
		console.log(e);
		return c.json({ msg: e.message });
	}
});





export { query_controller };
