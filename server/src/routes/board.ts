import { Hono } from "hono";
import { db } from "../repository/db";
import { JARVIS_URL } from "../shared";

const board_controller = new Hono();
board_controller.get("/", (c) => {
	const data = db
		.query(`
		 SELECT json_object(
			'id', id, 
			'name', name,	
			'columns', (
					SELECT json_group_array(
						json_object(
							'id', col.id, 
							'name', col.name, 
							'boardId', col.boardId
						)
					)
					FROM column AS col
					WHERE col.boardId = b.id)
		) AS record
		FROM board as b;`)
		.all();
	const _data = data.map((d: any) => JSON.parse(d.record));
	return c.json({ data: _data });
});

//detail

board_controller.get("/:id", (c) => {
	const id = c.req.param("id");
	const data = db
		.query(`
		 SELECT json_object(
			'id', id, 
			'name', name,	
			'columns', (
					SELECT json_group_array(
						json_object(
							'id', col.id, 
							'name', col.name, 
							'boardId', col.boardId
						)
					)
					FROM column AS col
					WHERE col.boardId = b.id)
		) AS record
		FROM board as b
		WHERE b.id = $id;`)
		.get({ $id: id });

	return c.json({ data: JSON.parse(data.record as string) });
});

// create
board_controller.post("/", async (c) => {
	return c.req.json().then((payload) => {
		const { name, color, accountId } = payload;
		const data = db
			.query(`
				INSERT INTO "board" ("name", "color", "accountId" )
				VALUES ($name, $color, $accountId)
				RETURNING id;`)
			.get({
				$name: name,
				$color: color,
				$accountId: Number.parseInt(accountId),
			});

		return c.json({ data });
	});
});

board_controller.post("/ask", async (c) => {
	try {
		const body = await c.req.json();
		return fetch(`${JARVIS_URL}agent`, {
			method: 'POST',
			body: JSON.stringify(body),
			headers: {
				'Content-Type': 'application/json'
			}
		})
	} catch (e) {
		return c.json({ msg: "oups" });
	}
});
//delete
board_controller.put("/:id", async (c) => {
	const id = c.req.param("id");
	return c.req.json().then((payload) => {
		const { name, color } = payload;
		const data = db
			.query(`
				UPDATE "board" 
				SET name=$name, color=$color, accountId=$accountId
				WHERE id = $id
				RETURNING id, name, color, accountId, createdAt`)
			.get({
				$name: name,
				$color: color,
				$accountId: 1,
				$id: Number.parseInt(id),
			});

		return c.json({ data });
	});
});
board_controller.delete("/:id", (c) => {
	const id = c.req.param("id");

	const data = db
		.query(`
			DELETE FROM "board" 
			WHERE id = $id
			RETURNING id`)
		.get({ $id: Number.parseInt(id) });

	return c.json({ data });
});

export { board_controller };
