import { Hono } from "hono";
import { db } from "../repository/db";

const column_controller = new Hono();

//all
column_controller.get("/", (c) => {
	const { boardId } = c.req.param() as { boardId: string };
	const data = db
		.query(`
				SELECT json_object(
					'id', id,
					'name', name,
					'position', position,
					'board_id', boardId,
					'items', (
															SELECT json_group_array(
																json_object(
																	'id', id,
																	'title', title,
																	'content',content,
																	'position', position,
																	'column_id', columnId
																	)	
															)
															FROM item as itm 
															WHERE col.id =itm.columnId
															ORDER BY itm.position ASC
													)
				) as items
				FROM column AS col 
				WHERE boardId=$boardId
				ORDER BY position ASC
				;
			`)
		.all({ $boardId: Number.parseInt(boardId) });

	const _data = data
		.map((d: any) => JSON.parse(d.items))
		.map((d: any) => {
			return {
				...d,
				items: d.items.sort((a: any, b: any) => a.position - b.position),
			};
		});
	return c.json({ data: _data });
});

//detail

column_controller.get("/:colId", (c) => {
	const { boardId, colId } = c.req.param() as {
		boardId: string;
		colId: string;
	};
	const data = db
		.query(`
				SELECT json_object(
					'id', id,
					'name', name,
					'position', position,
					'board_id', boardId,
					'items', (
															SELECT json_group_array(
																json_object(
																	'id', id,
																	'title', title,
																	'content',content,
																	'position', position,
																	'column_id', columnId
																	)	
															)
															FROM item as itm 
															WHERE itm.columnId = $colId
															ORDER BY itm.position 
													)
				) as items
				FROM column AS col 
				WHERE boardId=$boardId;
	`)
		.get({
			$boardId: Number.parseInt(boardId),
			$colId: Number.parseInt(colId),
		});
	const _data = data
		.map((d: any) => JSON.parse(d.items))
		.map((d: any) => {
			return {
				...d,
				items: d.items.sort((a: any, b: any) => a.position - b.position),
			};
		});
	return c.json({ data: _data });
});

// create
column_controller.post("/", async (c) => {
	return c.req.json().then((payload) => {
		const { name, position, boardId } = payload;
		const data = db
			.query(`
			INSERT INTO "column" ("name", "position", "boardId" )
			VALUES ($name, $position, $boardId)
			RETURNING id;`)
			.get({
				$name: name,
				$position: position,
				$boardId: boardId,
			});

		return c.json({ data });
	});
});
//delete
column_controller.put("/:id", async (c) => {
	const id = c.req.param("id");
	return c.req.json().then((payload) => {
		const { name, position, boardId } = payload;
		const data = db
			.query(`
			UPDATE "column" 
			SET name=$name,position=$position, boardId=$boardId
			WHERE id = $id
			RETURNING id, name, position, boardId`)
			.get({
				$id: id,
				$name: name,
				$position: position,
				$boardId: boardId,
			});
		return c.json({ data });
	});
});
column_controller.delete("/:id", (c) => {
	const id = c.req.param("id");

	const data = db
		.query(`
			DELETE FROM "column" 
			WHERE id = $id
			RETURNING id`)
		.get({
			$id: id,
		});

	return c.json({ data });
});

export { column_controller };
