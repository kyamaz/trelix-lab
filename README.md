## Dev container

### Setup
[source](https://cadu.dev/running-neovim-on-devcontainers/)
mount things than not work. Added in the .devcontainer, docker-compose.yaml for now
basic  workflow
-  `npx @devcontainers/cli build --workspace-folder .`
-  `npx @devcontainers/cli up --workspace-folder .`
-  `docker exec -it trellix-solidstart-1 sh`
-  `nvim .` inside the container, install supermaven then tree sitter/mason install pyright
#-  `npx @devcontainers/cli exec --workspace-folder . nvim`-->mount deps not working
-  `docker-compose down`

bonus:
- when building the devcontainer, nvim fully setup have to run up at least twice(marven then Treesitter)

interesting links
[feature](https://containers.dev/features)

and more docs on devcontainers
[source](https://github.com/devcontainers/cli)
[docs](https://code.visualstudio.com/docs/devcontainers/containers)
[docs](https://containers.dev/)


#### TODO
for now, running nvim from devcontainercli is useless because deps are read to host machine instead of container
so, run nvim inside each container
which means everytime switching between services
- update devcontainer.json to use SERVICE
- update docker-compose.yaml to use SERVICE
- run devcontainercli build
- reinstall nvim
#### Note:
added node to devcontainer.json, as lots of lsp depends on npm(pyright)


which is super annoying. at least, lsp is working

- [ ] everytime  have  to install pyright  && location import
- [ ] run exec nvim twice to fully setup nvim
- [ ] learn how to configure devcontainers
- [ ] add neovim config to devcontainer without having to declare it in the docker-compose.yaml
- [ ] maybe persist the devcontainer state( nvim plugins, etc)
